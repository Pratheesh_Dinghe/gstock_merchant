import { Component, OnInit, Input, Output, EventEmitter, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
interface Config {
    variants: Array<any>;
    source: LocalDataSource;
    default: {
        price: number
        stock: number
        sku: string
    };
}
import { CustomValidatorsService } from '../../shared/validators/custom-validator.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-variant-popout',
    templateUrl: 'variant-popout.component.html',
    styleUrls: ['variant-popout.component.scss']
})

export class VariantPopoutComponent implements OnInit {
    @Input('config') config: Config;
    @Output('newVariants') newVariantsEmitter = new EventEmitter<any>();
    private variantForm: FormGroup;
    private formArray;
    private generalForm: FormGroup;
    private generalArray;
    constructor(private _toastr: ToastrService) {

    }

    ngOnInit() {
        const { variants } = this.config;
        if (variants.length) {
            this.formArray = variants.map(
                (v) => this.transformVariantArray(v.name, v.value)
            );
            this.variantForm = this.formizeArray(variants);
            this.generalArray = [{
                formGroupDisplayName: 'Detail',
                formGroupName: 'detail',
                formControls: [
                    {
                        displayName: 'Price',
                        formControlName: 'price',
                        type: 'text',
                        set: []
                    },
                    {
                        displayName: 'Stock',
                        formControlName: 'stock',
                        type: 'text',
                        set: []
                    },
                    {
                        displayName: 'SKU',
                        formControlName: 'sku',
                        type: 'text',
                        set: []
                    }]
            }];
            const { price, stock, sku } = this.config.default || { price: '', stock: '', sku: '' };
            this.generalForm = new FormGroup({
                detail: new FormGroup({
                    price: new FormControl(price, [Validators.required, CustomValidatorsService.numeric()]),
                    stock: new FormControl(stock, [Validators.required, CustomValidatorsService.integer()]),
                    sku: new FormControl(sku, Validators.required)
                })
            });
        }
    }
    private formizeArray(array) {
        if (!array) { return; }
        const formGroup = {};
        array.forEach(a => undefined !== a.value ? formGroup[a.name] = this.formizeArray(a.value) : formGroup[a] = new FormControl(''));
        return new FormGroup(formGroup);
    }
    private transformVariantArray(name, value) {
        return {
            'formGroupDisplayName': name,
            'formGroupName': name,
            'formControls': value.map(
                a => ({
                    displayName: a,
                    formControlName: a,
                    type: 'checkbox',
                    set: []
                })
            )
        };

    }
    private combineVariant(variantArray: Array<any>) {
        const vA = variantArray.filter(a => a.length);
        if (vA.length === 1) {
            return vA[0];
        }
        return vA.reduce(
            (a, b) => {
                const holder = [];
                a.forEach((aE) => {
                    b.forEach((bE) => {
                        holder.push(aE + '/' + bE);
                    });
                });
                return holder;
            }
        );
    }

    private async addVariant() {
        const { source } = this.config;
        const currentVariants = await source.getAll();
        const form = this.variantForm.value;
        const detail = this.generalForm.value.detail;
        const options = Object.keys(form)
            .map(field => {
                const value = Object.keys(form[field]).filter(s => form[field][s]);
                if (value.length) {
                    return {
                        option: field,
                        value
                    };
                }
            })
            .filter(e => e);
        if (!options.length) { return; }
        const optionsJoined = options.map(o => o.option).join('/');
        const variantArray = options.map(e => e.value);
        const newVariant = this.combineVariant(variantArray).map(
            e => ({
                option_name: optionsJoined,
                option_value: e,
                ...detail
            }));

        const variants = newVariant.filter((newv, i) => {
            if (!currentVariants.length) { return true; }
            return currentVariants.find(cur => cur.option_value === newv.option_value) ? false : true;
        });
        if (!variants.length) { return this._toastr.error('You have added these variants. Please edit details directly in the table.'); }
        this.newVariantsEmitter.emit(variants);
    }
    ngOnChanges(changes) {
        const { price, stock, sku } = this.config.default || { price: '', stock: '', sku: '' };
        this.generalForm = new FormGroup({
            detail: new FormGroup({
                price: new FormControl(price),
                stock: new FormControl(stock),
                sku: new FormControl(sku)
            })
        });
    }
}
