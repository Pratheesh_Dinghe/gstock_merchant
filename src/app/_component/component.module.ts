import { NgModule } from '@angular/core';

import { VariantPopoutComponent } from './variant-popout/variant-popout.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilComponentsModule } from '../theme/util-components/util.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        UtilComponentsModule
    ],
    exports: [VariantPopoutComponent],
    declarations: [VariantPopoutComponent],
    providers: [],
})
export class ComponentModule { }
