import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoaderService {
    public commandEmitter = new Subject<string>();
    constructor(private httpClient: HttpClient) {

    }

    show() {
        this.commandEmitter.next('show');
    }
    hide() {
        this.commandEmitter.next('hide');
    }
}
