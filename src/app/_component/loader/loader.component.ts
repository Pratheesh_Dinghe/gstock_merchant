import { Component, ViewEncapsulation, Renderer2, ViewChild, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
    selector: 'gs-loader',
    templateUrl: 'loader.component.html',
    styleUrls: ['./loader.component.scss']
})

export class LoaderComponent implements OnInit {
    @ViewChild('preloader') preloader;
    private visibility = false;
    constructor(
        private _rd2: Renderer2,
        private _loader: LoaderService
    ) {
        _loader.commandEmitter.subscribe((command) => {
            switch (command) {
                case 'show':
                    this.visibility = true;
                    break;
                case 'hide':
                    this.visibility = false;
                    break;
                default: break;
            }
        }
        );
    }

    ngOnInit() { }

}
