import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {
    getHeaders(): any {
        throw new Error("Method not implemented.");
    }
    public userLogin = new Subject();
    public firstLoad = false;
    private url = environment.apiUrl;
    // store the URL so we can redirect after logging in
    public commissionList = [];
    public redirectUrl: string;
    constructor(public http: HttpClient) {
    }

    public setUserDetails(token, uid) {
        this.firstLoad = true;
        localStorage.setItem('uid', uid);
        localStorage.setItem('token', token);
        this.userLogin.next(true);
    }
    public setCommissionList(data) {
        this.commissionList = data;
    }
    public getCommissionList() {
        return this.commissionList;
    }
    public getUser() {
        const uid = localStorage.getItem('uid');
        console.log(uid);
        if (!uid) {
            alert('Please login again');
            return this.userLogin.next(false);

        }
        return uid;
    }
   public getToken() {
        const token = localStorage.getItem('token');
        if (!token) {
            alert('Please login');
            return this.userLogin.next(false);
        }
        return token;
    }
    public getAuthUploadHeader(){
        return { 'authorization': 'Bearer ' + this.getToken() };
    }
    public getAuthHeader() {
        return new HttpHeaders({ 'authorization': 'Bearer ' + this.getToken() });
    }
    public login(body) {
       return this.http.post(this.url + '/user/login', body);

    }

    public logout() {
        this.firstLoad = false;
        localStorage.clear();
        this.userLogin.next(false);
        return;
    }

    public signup(body) {
        return this.http.post(this.url + '/user/register', body);
    }

    public reg(body) {
        console.log(body);
        return this.http.post(this.url + '/user/reg', body);
    }
    public authState() {
        return this.http.get(this.url + '/auth', { headers: this.getAuthHeader(), responseType: 'text' });
    }

    public forget(email) {
        console.log(email);
        return this.http.post(`${this.url}/user/forgot/email`, { email: email }, { responseType: 'text' });
    }

    public reset(detail) {
        console.log(detail);
        return this.http.post(`${this.url}/user/forgot/reset`, detail, { responseType: 'text' });
    }
}
