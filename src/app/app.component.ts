import { Component, ViewEncapsulation, Renderer2, ViewChild } from '@angular/core';
import { AuthService } from './shared/auth/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from './_component/loader/loader.service';



@Component({
  selector: 'az-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  constructor(
    public _authService: AuthService,
    public _loader: LoaderService,
    public toast: ToastrService,
    private router: Router
  ) {

    this._authService.userLogin.subscribe(userlogin => {
      this._loader.show();
      if (userlogin) {
        this.router.navigate(['pages']).then(a => {
          if (this._authService.firstLoad) {
            this.toast.success(`Welcome to Gtock merchant panel. Please update your personal information if you have not done so.`);
            this._authService.firstLoad = false;
          }
          this.router.navigate(['/pages']);
          this._loader.hide();
        });
      } else {
        this.router.navigate(['/login']);
        this._loader.hide();
      }
    });
  }
  ngOnInit() {

  }
}
