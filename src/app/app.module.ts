import { RegisterLoginResetModule } from "./register-login-reset/register-login-reset.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { ToastrModule } from "ngx-toastr";
import { LoaderModule } from "./_component/loader/loader.module";
import "pace";
import { UtilComponentsModule } from "./theme/util-components/util.module";

import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AgmCoreModule } from "@agm/core";
import { environment } from "../environments/environment";
import { routing } from "./app.routing";
import { AppConfig } from "./app.config";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AppComponent } from "./app.component";
import { ErrorComponent } from "./pages/error/error.component";
import { HttpModule, JsonpModule } from "@angular/http";
import { AppGuard } from "./app-guard.service";
import { AuthService } from "./shared/auth/auth.service";

// import "froala-editor/js/froala_editor.pkgd.min.js";
// import { FroalaEditorModule, FroalaViewModule } from "angular2-froala-wysiwyg";

import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";

@NgModule({
  declarations: [AppComponent, ErrorComponent],
  imports: [
    HttpClientModule,
    HttpModule,
    JsonpModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDe_oVpi9eRSN99G4o6TwVjJbFBNr58NxE"
    }),
    RegisterLoginResetModule,
    UtilComponentsModule,
    LoaderModule,
    routing
  ],
  providers: [AppConfig, AuthService, AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
