import { errorHandler } from '../../_helper/errorHandler';
import { AuthService } from '../../shared/auth/auth.service';
import { environment } from '../../../environments/environment';
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from '../../_component/loader/loader.service';
import { ToastrService } from 'ngx-toastr';
import { RegisterService } from '../register.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'az-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    public form: FormGroup;
    public forget: false;
    public url: string;
    status = 0;
    email; password;
    loading: boolean;
    constructor(
        public router: Router,
        public fb: FormBuilder,
        public toast: ToastrService,
        public _authService: AuthService,
        public _loader: LoaderService,
        private _registerService: RegisterService,
    ) {
        this.form = fb.group({
            email: ['', Validators.compose([Validators.required, emailValidator])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
            // user_group: ['merchant']
        });
        this.url = this.router.url.split('/')[1];
    }

    public onSubmit(): void {

        this._loader.show();
        if (!this.forget) {
            if (!this.form.valid) { return alert('Invalid entry'); }
            this._authService.login(this.form.value).subscribe(
                (data: any) => {
                    console.log(data);
                    const { user_id, token } = data;
                    this._authService.setUserDetails(token, user_id);
                    this._loader.hide();
                },
                (err) => {
                    this.toast.error(err.error.message);
                    this._loader.hide();
                });
        } else {
            if (!this.form.controls['email'].valid) {
                return alert('Invalid entry');
            }

            this.loading = true;
            this._authService.forget(this.form.value['email']).subscribe(
                data => {
                    alert("Please check your mail!");
                    this.loading = false;
                },
                (err: HttpErrorResponse) => {
                    if (400 === err.status) {
                        alert("Please enter valid email id");
                    }
                }
            );
        }

        console.log(this.form.value);
        //  this._authService.forget(this.form.value['email']).subscribe(
        //      s => this._loader.hide(),
        //      e => this._loader.hide()
        //  );

        //  this._registerService.LoginFetch(this.form.value).subscribe(loggedInUser => {
        //      console.log("- login success");
        //     console.log(loggedInUser);
        //      this.status = 0;
        //    //  this._authService.setUserDetails(token, user_id);


        // //   //  alert("Success");
        //  this.router.navigate(['../pages/dashboard']);
        //  }, err => {
        //      this.status = 1;
        // //    // alert("Error" + err.error);
        // })

    }
}
/*  console.log(data["message"]);
           console.log(data['suppliers'][0].username);
           console.log(data['suppliers'][0].Password);
            if(data['message']=="Login failed"){
             this.status=1;
            alert("Login failed");
            }
            if(data['message']=="Logined Succesfully"){    
            this.status=0;   
            alert("Logined Succesfully");
           this.router.navigate(['/register']);
       }
       */




export function emailValidator(control: FormControl): { [key: string]: any } {
    const emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}
