
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { RegisterLoginResetComponent } from './register-login-reset.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetComponent } from './reset/reset.component';
import { RegisterService } from './register.service';

export const routes = [
  {
    path: '', component: RegisterLoginResetComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'reset/:email', component: ResetComponent }]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    RegisterLoginResetComponent,
    LoginComponent,
    RegisterComponent,
    ResetComponent
  ],
  exports: [
    LoginComponent,
    RegisterComponent,
    ResetComponent
  ],
  providers: [RegisterService ]
})

export class RegisterLoginResetModule { }
