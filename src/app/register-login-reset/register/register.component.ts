import { errorHandler } from "../../_helper/errorHandler";
import { AuthService } from "../../shared/auth/auth.service";
import { Component, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
//import { RegisterService } from '../register.service';
import {
  FormGroup,
  FormControl,
  AbstractControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { RegisterService } from "../register.service";

@Component({
  selector: "az-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent {
  public router: Router;
  public form: FormGroup;
  Uen_no;
  activeUserId: any;
  first_name: any;
  confirm_password: any;
  password;
  email;

  status = 0;

  constructor(
    router: Router,
    fb: FormBuilder,
    public _authService: AuthService,
    private _registerService: RegisterService
  ) {
    this.router = router;
    this.form = fb.group(
      {
        Uen_no: ["", Validators.required],
        first_name: ["", Validators.required],
        email: ["", Validators.compose([Validators.required, emailValidator])],
        password: [""],
        confirm_password: [""],

        user_group: ["merchant"]
      },
      { validator: matchingPasswords("password", "confirm_password") }
    );
  }

  onSubmit(values: Object): void {
    console.log("This form", this.form.value);
    console.log(this.password, this.email);

    if (this.form.valid) {
      console.log("This form", this.form.value);
      this.form.value["id"] = this.activeUserId;
      console.log(this.form.value["id"]);
      this._authService.reg(this.form.value).subscribe(
        data => {
          console.log("Successful", data);
          alert("Successfully registered!");
          this.router.navigate(["/login"]);
        },
        err => errorHandler(err)
      );
      //    this._registerService.updateMerchantDetails(this.form.value).subscribe(respo => {
      //       console.log(respo);
      //   });
    }
    /*    this._authService.reg(this.form.value).subscribe(respo => {
            console.log(respo);
            this.router.navigate(['/login']);
        });*/
  }

  itemNameChange() {
    // alert("hi");
    // console.log(this.Uen_no);
    this._registerService
      .merchantnameFetch({ id: this.Uen_no })
      .subscribe(data => {
        console.log(data);
        if (data["message"] == "add different.") {
          this.status = 1;
          //   alert("UEN/IC no is worng");
        }
        if (data["message"] == "okay.") {
          this.status = 0;
          this.form.controls["first_name"].setValue(data["suppliers"][0].name);
          //alert("okay");
        }
      });
  }
}

export function emailValidator(control: FormControl): { [key: string]: any } {
  const emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}

export function matchingPasswords(
  passwordKey: string,
  passwordConfirmationKey: string
) {
  return (group: FormGroup) => {
    const password = group.controls[passwordKey];
    const passwordConfirmation = group.controls[passwordConfirmationKey];
    if (password.value !== passwordConfirmation.value) {
      return passwordConfirmation.setErrors({ mismatchedPasswords: true });
    }
  };
}
