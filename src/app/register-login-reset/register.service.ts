import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { AuthService } from "../shared/auth/auth.service";
//import { AuthService } from "../shared/auth.service";

@Injectable()
export class RegisterService {
  private url = environment.apiUrl;
  private adminurl = environment.adminurl;
  private headers;
  public authHeader;
  constructor(
    private httpClient: HttpClient,
    private _authService: AuthService
  ) {
    //   this.headers = _authService.getHeaders();
    //  this.headers = _authService.getHeaders();
  }

  merchantnameFetch(body) {
    console.log("merchant IC", body);
    alert(this.url + "/admin/user/getname");
    return this.httpClient.post(this.adminurl + "/admin/user/getname", body);
    //  headers: this.authHeader
  }
  public updateMerchantDetails(body) {
    return this.httpClient.post(
      this.adminurl + "/admin/user/postdetails",
      body,
      {}
    );
  }
  public LoginFetch(body) {
    //  return this.httpClient.post(this.url + "/admin/user/fetchlogin")
    return this.httpClient.post(
      this.adminurl + "/admin/user/fetchlogin",
      body,
      {}
    );
  }
}
