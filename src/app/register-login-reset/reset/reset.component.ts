import { Component, OnInit } from '@angular/core';
import { errorHandler } from '../../_helper/errorHandler';
import { AuthService } from '../../shared/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'az-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  public form: FormGroup;
  public resetDetail;
  mail: any;
  token: any;
  email: any;
  loading: boolean;
  constructor(
    public router: Router, fb: FormBuilder, public _authService: AuthService, public _ar: ActivatedRoute
  ) {

    this.form = fb.group({
      password: ['', Validators.required],
      confirm_password: ['', Validators.required],
      user_group: ['merchant']
    }, { validator: matchingPasswords('password', 'confirm_password') });
   this._ar.params.subscribe(
       p => {
         this.resetDetail = p
         console.log("reset",this.resetDetail);

        console.log(p);
       const { email } = p;
         this.mail = email;
       console.log(this.mail);
       // alert(this.mail);
      //    const { email, token } = p;
      //     this.email = email;
      // this.token = token;
      } )

   //  $2a$10$P9mGFLy06b8RIl.GEiHDkumTbhMgYUa8LCzUxx04G2U833QUuxtay

  }

  ngOnInit() {
  }

  private onSubmit(values: Object): void {
    console.log(this.form.value)
    if (this.form.valid) {
      this.loading = true;
      this._authService.reset(
        {...this.resetDetail,new_pass: this.form.value['password'],email: this.mail}).subscribe(
          (data) => {
            this.loading = false;
            console.log("DATA", data)
            alert('Password changed!');
            this.router.navigate(["/login"]);
          },
          (err) => errorHandler(err),
        )
    }
  }
}

export function emailValidator(control: FormControl): { [key: string]: any } {
  const emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
  return (group: FormGroup) => {
    const password = group.controls[passwordKey];
    const passwordConfirmation = group.controls[passwordConfirmationKey];
    if (password.value !== passwordConfirmation.value) {
      return passwordConfirmation.setErrors({ mismatchedPasswords: true })
    }
  }
}
