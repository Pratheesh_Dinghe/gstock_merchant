
export function addTimestamp(url: string) {
    if (url.lastIndexOf('?ts=') > 0) {
        return url.substr(0, url.lastIndexOf('?ts=')) + '?ts=' + Date.now();
    } else {
        return url + '?ts=' + Date.now();
    }
}
