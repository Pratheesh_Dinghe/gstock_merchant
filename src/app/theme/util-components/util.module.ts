import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ControlMessagesComponent } from './control-messages/control-messages.component';
import { ExpandableCardComponent } from './expandable-card/expandable-card.component';
import { DirectivesModule } from '../../theme/directives/directives.module';


@NgModule({
    imports: [
        CommonModule,
        DirectivesModule
    ],
    declarations: [
        ControlMessagesComponent,
        ExpandableCardComponent


    ],
    exports: [
        ControlMessagesComponent,
        ExpandableCardComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UtilComponentsModule { }
