import { Component, Input } from '@angular/core';


@Component({
    selector: 'expandable-card',
    templateUrl: './expandable-card.component.html',
})
export class ExpandableCardComponent {
    @Input()public header = 'header';

    @Input()public closable = false;
}
