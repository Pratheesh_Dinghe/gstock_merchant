import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CustomValidatorsService } from '../../../shared/validators/custom-validator.service';


@Component({
  selector: 'control-messages',
  template: `<div class="error-message" *ngIf="errorMessage !== null">{{errorMessage}}</div>`,
  styleUrls: ['./control-messages.component.css']
})
export class ControlMessagesComponent {
  @Input() control: FormControl;
  constructor() { }

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return CustomValidatorsService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }

    return null;
  }
}
