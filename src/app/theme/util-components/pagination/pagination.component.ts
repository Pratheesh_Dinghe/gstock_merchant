import { EventEmitter, Input, Component, ViewEncapsulation, OnInit, Output } from '@angular/core';

@Component({
  selector: 'az-pagination',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Output('number') private numberEmitter: EventEmitter<number> = new EventEmitter();
  private pages = 0;
  private jump = 4;
  private head = 0;
  public inited = false;
  private display: Array<number> = [];

  constructor() {
  }

  ngOnInit() {

  }
  public init(totalPages) {
    if (!this.inited) {
      this.head = 0;
      this.pages = totalPages;
      if (this.pages > this.jump) {
        console.log(this.pages, this.jump);
        this.shiftDigit(0, this.jump);
      } else {
        this.shiftDigit(0, this.pages - this.head);
      }
      this.inited = true;
    }
  }
  private transitTo(direction) {
    const remainingPages = this.pages - this.head;
    if (direction === 'previous' && this.head !== 0) {
      this.head -= this.jump;
      return this.shiftDigit(this.head, this.jump);
    }
    if (direction === 'next' && remainingPages > this.jump) {
      if (remainingPages > this.jump * 2) {
        this.head += this.jump;
        this.shiftDigit(this.head, this.jump);
        return;
      } else {
        this.head += this.jump;
        this.shiftDigit(this.head, remainingPages - this.jump);
        return;
      }
    }
  }
  private navigateToPage(number) {
    this.numberEmitter.emit(number);
  }
  private shiftDigit(head: number, box: number) {
    const array = [];
    for (let i = 1; i <= box; i++) {
      array.push(head + i);
    }
    this.display = array;
  }
}
