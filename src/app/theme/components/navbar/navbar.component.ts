import { AuthService } from '../../../shared/auth/auth.service';
import { Component, ViewEncapsulation } from '@angular/core';
import { AppState } from '../../../app.state';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { SidebarService } from '../sidebar/sidebar.service';
import { errorHandler } from '../../../_helper/errorHandler';
import { Router } from '@angular/router';
import { addTimestamp } from '../../../_helper/addTimestamp';

@Component({
    selector: 'az-navbar',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    providers: [SidebarService]
})

export class NavbarComponent {
    public url = environment.apiUrl;
    public authHeader;
    public isMenuCollapsed = false;
    public merchantProfile = {};
    public profileImage = 'assets/img/no-image.jpg';

    constructor(
        private _state: AppState,
        private _sidebarService: SidebarService,
        private _authService: AuthService,
        private http: HttpClient,
        private router: Router) {
        this.authHeader = this._authService.getAuthHeader();
        this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
            this.isMenuCollapsed = isCollapsed;
        });

        _state.subscribe('update.profile', (data) => {
            console.log('update.profile', data);
            this.getMerchantProfile().subscribe(
                (data: any) => {
                    if (data['commission']) {
                        this._authService.setCommissionList(data['commission']);
                    }
                    this.merchantProfile = data;
                    const baseURL = `${environment.apiUrl}/uploads/user/${data._id}`
                    this.profileImage = addTimestamp(`${environment.apiUrl}/uploads/uploads/user/${data._id}/profile_picture.png`);
                },
                onError => {
                    errorHandler(onError);
                }
            );
        })
    }


    ngOnInit() {
        this.getMerchantProfile().subscribe(
            (data: any) => {
                if (data['commission']) {
                    this._authService.setCommissionList(data['commission']);
                }
                this.merchantProfile = data;
                const baseURL = `${environment.apiUrl}/uploads/user/${data._id}`
                this.profileImage = addTimestamp(`${environment.apiUrl}/uploads/uploads/user/${data._id}/profile_picture.png`);
            },
            onError => {
                errorHandler(onError);
            }
        )
    }
    public logout() {
        this._authService.logout()
    }
    public closeSubMenus() {
        /* when using <az-sidebar> instead of <az-menu> uncomment this line */
        // this._sidebarService.closeAllSubMenus();
    }
    public toggleMenu() {
        this.isMenuCollapsed = !this.isMenuCollapsed;
        this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    }
    public getMerchantProfile() {
        return this.http.get(this.url + '/user/account/detail?select=profile/company/contact/location/store/commission', { headers: this.authHeader })

    }





}
