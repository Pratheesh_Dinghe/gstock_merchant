import { Component, ViewEncapsulation } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { MessagesService } from './messages.service';
import { Subject } from 'rxjs/Subject'
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/auth/auth.service';



@Component({
    selector: 'az-messages',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./messages.component.scss'],
    templateUrl: './messages.component.html',
    providers: [MessagesService]
})

export class MessagesComponent {
    public messages: Array<Object>;
    news: Observable<any[]>;
    public orderUrl = 'https//www.baidu.com';
    public notifications: Array<Object>;
    public tasks: Array<Object>;
    public newCount = 0;
    newsRef: AngularFireList<any>;
    newsData: Observable<any[]>;

    // const size$ = new Subject<string>();


    constructor(private _messagesService: MessagesService, db: AngularFireDatabase, private router: Router,
        private _authService: AuthService
    ) {
        this.messages = _messagesService.getMessages();
        this.notifications = _messagesService.getNotifications();
        this.tasks = _messagesService.getTasks();


        // const queryObservable = this.size$.switchMap(size =>
        //     db.list('news/'+environment.uid, ref => ref.orderByChild('create_date')).valueChanges()
        //   );


        this.newsRef = db.list('news/' + this._authService.getUser(), ref => ref.orderByChild('create_date').limitToLast(10));
        this.newsRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        }).subscribe(
            (success: any) => {
                this.newCount = 0;
                this.news = success;
                console.log('This.news', success);
                this.news = success;
                this.news.forEach(
                    data => {
                        if (data['read'] === false) {
                            this.newCount += 1;
                        }
                    }
                )
            });
        //     this.newsRef.valueChanges().subscribe(
        //     (success:any)=>
        //     {
        //         console.log("This.news",success);
        //         this.news=success;
        //         this.news.forEach(
        //             data=>{
        //                 if(data["read"]==false)
        //                 {
        //                     this.newCount+=1;
        //                 }
        //             }
        //         )

        //     }
        // )

        //  console.log("News", this.news)
    }

    public updateNews(data) {
        console.log('Update', data);
        this.newsRef.update(
            data.key, { read: true }
        )
        this.router.navigate(['pages/order/order-detail/' + data.order_id])


    }

}
