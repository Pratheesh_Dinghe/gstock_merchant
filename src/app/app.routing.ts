import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ErrorComponent } from "./pages/error/error.component";
import { AppGuard } from "./app-guard.service";
export const routes: Routes = [
  // { path: '', loadChildren: 'app/register-login-reset/register-login-reset.module#RegisterLoginResetModule' },
  {
    path: "pages",
    loadChildren: "app/pages/pages.module#PagesModule",
    canLoad: [AppGuard]
  },

  {
    path: "**",
    component: ErrorComponent
  }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
  preloadingStrategy: PreloadAllModules,
  useHash: true
});
