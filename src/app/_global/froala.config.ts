import { environment } from "../../environments/environment";

export const froalaConfig = {
    key: environment.froalaEditorKey,
    placeholderText: 'Describe your product',
    // Set the image upload parameter.
    imageUploadParam: 'productPic',
}