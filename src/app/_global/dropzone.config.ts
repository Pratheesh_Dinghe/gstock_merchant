
import { environment } from '../../environments/environment.prod';
import { IMAGE_DIMENSION } from './image.config';
export const dropzoneConfig = {
    paramName: 'productPic',
    dictDefaultMessage: 'Drop your product picture here. Max 12 photos allowed',
    maxFilesize: 5,
    acceptedFiles: 'image/*',
    maxFiles: 12,
    init: function () {
        this.on('thumbnail', function (file) {
            // Do the dimension checks you want to do
            if (file.width < IMAGE_DIMENSION.WIDTH || file.height < IMAGE_DIMENSION.HEIGHT) {
                file.rejectDimensions(file.width, file.height);
            } else {
                file.acceptDimensions();
            }
        });
    },
    accept: (file, done) => {
        file.acceptDimensions = done;
        file.rejectDimensions = (width, height) => {
            done(`Invalid dimension.Detected ${width}x${height}. Require at least ${IMAGE_DIMENSION.WIDTH}x${IMAGE_DIMENSION.HEIGHT}`
            );
        };
    },
};
