import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { PromotionService } from '../promotion.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatepickerOptions } from 'ng2-datepicker';


@Component({
    selector: 'az-promotion-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './promotion-detail.component.html',
    styleUrls: ['./promotion-detail.scss']
})

export class PromotionDetailComponent implements OnInit {
    private discountForm1: FormGroup;
    private discountForm2: FormGroup;
    private discountForm3: FormGroup;
    private options: DatepickerOptions;
    private promotionID: string;
    private disOption = [
        {
            name: 'Percentage discount',
            value: 'pd'
        },
        {
            name: 'Fixed Amount',
            value: 'fa'
        },
        {
            name: 'Free Shipment',
            value: 'fs'
        }
    ];
    constructor(
        router: Router,
        public _promotionService: PromotionService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder
    ) {

        this.discountForm1 = formBuilder.group({
            discountCode: [''],
            discountOption: [''],
            discountValue: [0],
            miniOrder: [false],
            miniValue: [0]
        });
        this.discountForm2 = formBuilder.group({
            disApplies: [''],
            disSearch: ['']
        });

        this.discountForm3 = formBuilder.group({
            startDate: [new Date()],
            endDate: [new Date(Date.now() + 10000000000)]
        });

        this.options = {
            minYear: 1970,
            maxYear: 2030,
            displayFormat: 'MMM D[,] YYYY HH[:]MM[:]ss',
            barTitleFormat: 'MMMM YYYY',
            firstCalendarDay: 0 // 0 - Sunday, 1 - Monday
        };
    }

    ngOnInit() {

        this.activatedRoute.params.subscribe((params: Params) => {
            if (!params) {
                return
            }
            this.promotionID = params['id'];
            console.log('PromotionID ', this.promotionID);
            this._promotionService.getAllPromotion({ query: { '_id': this.promotionID } }).subscribe(data => console.log(data),
                err => errorHandler(err))
        })
    }

}
