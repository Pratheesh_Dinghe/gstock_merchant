import { errorHandler } from '../../../_helper/errorHandler';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, Ng2SmartTableModule } from 'ng2-smart-table';
import { PromotionService } from '../promotion.service';
import { Router } from '@angular/router';

@Component({
    selector: 'az-promotion-list',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './promotion-list.component.html',
    styleUrls: ['./promotion-list.component.scss']
})
export class PromotionListComponent implements OnInit {
    private source = new LocalDataSource();
    private settings = {
        actions: {
            add: false,
            edit: false,
            delete: false

        },


        columns: {
            promoCode: {
                title: 'Promo Code',

            },
            status: {
                title: 'Status'
            },
            used: {
                title: 'Used',

            },
            start: {
                title: 'start',

            },
            end: {
                title: 'end',

            }

        }
    };
    private promoData = [];

    constructor(private router: Router, private _promotionService: PromotionService) {
    }

    ngOnInit() {
        this._promotionService.getAllPromotion().subscribe(
            (data: any) => this.source.load(data),
            err => errorHandler(err)
        )
    }

    private statusChange(status?) {
        this._promotionService.getAllPromotion({ query: { 'status': status } }).subscribe(
            (data: any) => this.source.load(data),
            err => errorHandler(err)
        )

    }



    private selectPromo(event): void {
        this.router.navigate(['pages/promotion/promotion-detail/' + event.data._id])
    }


}
