import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Mail, MailService } from './mail.service';
import { AppState } from '../../app.state';

declare var gapi
@Component({
    selector: 'az-mail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './mail.component.html',
    styleUrls: ['./mail.component.scss'],
    providers: [MailService]
})
export class MailComponent {
    public mails: Observable<Mail[]>;
    public id: number;
    public type: string;
    public markAsRead = false;
    public markAsUnRead = false;
    public deleteChecked = false;
    public CLIENT_ID = '272812287991-4k77vpdi4hpkk46t3r091sc57g2aigcb.apps.googleusercontent.com';

    // Array of API discovery doc URLs for APIs used by the quickstart
    public DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'];

    // Authorization scopes required by the API; multiple scopes can be
    // included, separated by spaces.
    public SCOPES = 'https://www.googleapis.com/auth/gmail.readonly';
    public authorizeButton;
    public signoutButton
    constructor(private service: MailService,
        private route: ActivatedRoute,
        public router: Router,
        private state: AppState) {

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.id = this.route.snapshot.firstChild.params['id'];
                this.type = this.route.snapshot.firstChild.params['type'];
                setTimeout(() => {
                    jQuery('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' });
                });
            }
        });

    }
    ngAfterViewInit() {
        this.authorizeButton = document.getElementById('authorize-button');
        this.signoutButton = document.getElementById('signout-button');
        this.handleClientLoad()
    }


    public getBack() {
        if (this.type)
            this.router.navigate(['pages/mail/mail-list/' + this.type]);
        else
            this.router.navigate(['pages/mail/mail-list/inbox']);
    }

    public trash() {
        jQuery('[data-toggle="tooltip"]').tooltip('hide');
        this.service.getMail(this.id).then((mail) => {
            mail.trash = true;
            mail.sent = false;
            mail.draft = false;
            mail.starred = false;
        });
        this.router.navigate(['pages/mail/mail-list/inbox']);
    }

    public setAsRead() {
        this.markAsRead = !this.markAsRead;
        this.state.notifyDataChanged('markAsRead', this.markAsRead);
    }

    public setAsUnRead() {
        this.markAsUnRead = !this.markAsUnRead;
        this.state.notifyDataChanged('markAsUnRead', this.markAsUnRead);
    }

    public deleteCheckedMail() {
        this.deleteChecked = !this.deleteChecked;
        this.state.notifyDataChanged('deleteChecked', this.deleteChecked);
    }

    // Client ID and API key from the Developer Console




    /**
     *  On load, called to load the auth2 library and API client library.
     */
    public handleClientLoad() {
        gapi.load('client:auth2', this.initClient.bind(this));
    }

    /**
     *  Initializes the API client library and sets up sign-in state
     *  listeners.
     */
    public initClient() {
        gapi.client.init({
            discoveryDocs: this.DISCOVERY_DOCS,
            clientId: this.CLIENT_ID,
            scope: this.SCOPES
        }).then(() => {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);

            // Handle the initial sign-in state.
            this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            this.authorizeButton.onclick = this.handleAuthClick;
            this.signoutButton.onclick = this.handleSignoutClick;
        });
    }

    /**
     *  Called when the signed in status changes, to update the UI
     *  appropriately. After a sign-in, the API is called.
     */
    public updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
            this.authorizeButton.style.display = 'none';
            this.signoutButton.style.display = 'block';

            gapi.client.gmail.users.messages.list({
                'userId': 'me'
            })
                .then((res) =>
                    this.batchRequest(res.result.messages)
                        .then(data => {
                            console.log(data)
                            console.log(this.service.setEmail(Object.keys(data.result).map(key => { return data.result[key] })))
                        })
                )
        } else {
            this.authorizeButton.style.display = 'block';
            this.signoutButton.style.display = 'none';
        }
    }

    /**
     *  Sign in the user upon button click.
     */
    public handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
    }

    /**
     *  Sign out the user upon button click.
     */
    public handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
    }

    /**
     * Append a pre element to the body containing the given message
     * as its text node. Used to display the results of the API call.
     *
     * @param {string} message Text to be placed in pre element.
     */
    public appendPre(message) {
        const pre = document.getElementById('content');
        const textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
    }
    public batchRequest(array) {
        const batch = gapi.client.newBatch();

        array.forEach(item => {
            batch.add(
                gapi.client.gmail.users.messages.get({
                    'userId': 'me',
                    'id': item.id
                }))
        })
        return batch
    }

    /**
     * Print all Labels in the authorized user's inbox. If no labels
     * are found an appropriate message is printed.
     */

    // public listLabels() {
    //     gapi.client.gmail.users.messages.list({
    //         'userId': 'me'
    //     }).then((response) => {
    //         const labels = response.result;

    //         this.appendPre('Labels:');

    //         if (labels && labels.length > 0) {
    //             for (let i = 0; i < labels.length; i++) {
    //                 const label = labels[i];
    //                 this.appendPre(label.name)
    //             }
    //         } else {
    //             this.appendPre('No Labels found.');
    //         }
    //     });
    // }
    public listMessages(userId, query, callback) {
        const getPageOfMessages = function (request, result) {
            request.execute(function (resp) {
                result = result.concat(resp.messages);
                const nextPageToken = resp.nextPageToken;
                if (nextPageToken) {
                    request = gapi.client.gmail.users.messages.list({
                        'userId': userId,
                        'pageToken': nextPageToken,
                        'q': query
                    });
                    getPageOfMessages(request, result);
                } else {
                    callback(result);
                }
            });
        };
        const initialRequest = gapi.client.gmail.users.messages.list({
            'userId': userId,
            'q': query
        });
        getPageOfMessages(initialRequest, []);
    }

}
