import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';
import * as flatten from 'flatten-obj';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'az-order-overview',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './order-overview.component.html',
    styleUrls: ['./order-overview.component.scss'],
})

export class OrderOverviewComponent implements OnInit {
    private source = new LocalDataSource();
    private selected = [];
    private statusFilter = [];
    private data = [];
    private settings = {
        actions: {
            delete: false,
            edit: false,
            add: false
        },
        columns: {
            _id: {
                title: 'Order ID'
            },
            createdAt: {
                title: 'Date',
                valuePrepareFunction: (date) => {
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;
                }
            },
            'total.store_ap': {
                title: 'Total Amount'
            },
            status: {
                title: 'Status',
                valuePrepareFunction: (status) => {
                    const full = {
                        'GR': 'Good Received',
                        'DG': 'Delivering',
                        'AD': 'Awaiting Delivery'
                    }[status];
                    return full ? full : status;
                }
            }
        }
    };
    private searchForm: FormGroup;
    constructor(
        private router: Router,
        public _orderService: OrderService,
        public _fb: FormBuilder
    ) {
        this.searchForm = this._fb.group(
            {
                'start': [],
                'end': [],
                // 'status': [],
            }
        );

        this.statusFilter = [
            {
                name: 'All Orders',
                value: 'all'
            },
            {
                name: 'Awaiting Payment',
                value: 'ap'
            },
            {
                name: 'Awaiting Fulfillment',
                value: 'af'
            },
            {
                name: 'Awaiting Shipment',
                value: 'as'
            },
            {
                name: 'Goods Received',
                value: 'gr'
            }
        ];


    }
    ngOnInit(): void {
        this._orderService.getOrders().subscribe(
            (data: Array<any>) => this.source.load(data.map(order => flatten()(order))),
            err => errorHandler(err)
        );
    }

    private selectOrder(event): void {
        this.router.navigate(['pages/order/order-detail/' + event.data._id]);
    }

    private statusChange(status) {

        this.source.setFilter([{
            field: 'status', search: status

        }]);
    }

    private search() {
        const start = this.searchForm.value.start.split('-');
        const end = this.searchForm.value.end.split('-');
        const now = new Date();
        const offset = now.getTimezoneOffset() * 60;
        console.log(start, end);
        this._orderService.getOrders(`?start=${Date.UTC(start[0], start[1], start[2])}&end=${Date.UTC(end[0], end[1], end[2])}`).subscribe((s) => console.log(s));
    }

    // onSelect({ selected }) {
    //     console.log('Select Event', selected, this.selected);

    //     this.selected.splice(0, this.selected.length);
    //     this.selected.push(...selected);
    // }
    // onActivate(event) {
    //     console.log('Activate Event', event);
    // }


}
