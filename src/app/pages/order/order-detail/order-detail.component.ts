import {
    errorHandler
} from '../../../_helper/errorHandler';
import {
    Component,
    ViewEncapsulation,
    OnInit
} from '@angular/core';
import {
    Ng2SmartTableModule
} from 'ng2-smart-table';
import {
    OrderService
} from '../order.service';
import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';
import {
    FormControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import {
    LocalDataSource
} from 'ng2-smart-table';
import * as flatten from 'flatten-obj';
import * as jsPDF from 'jspdf';
@Component({
    selector: 'az-order-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})

export class OrderDetailComponent implements OnInit {
    private status;
    private commissionStatus;
    private shipmentToForm: FormGroup;
    private shipmentFromForm: FormGroup;
    private shipmentData = {};
    private order_id = '';
    private shipmentAgent = [];
    private shipment_id = '';
    private gstockComm;
    private orderItems;
    private orderStatus = [];
    private orderDetail = [];
    private buyer;
    private editShipment = false;
    private source = new LocalDataSource();
    private order;
    private settings = {
        actions: false,
        columns: {
            'name': {
                title: 'Product Name'
            },
            'option_value': {
                title: 'Variants'
            },
            'price': {
                title: 'Unit Price'
            },
            'discount_price': {
                title: 'Discount Price'
            },
            'qty': {
                title: 'Qty'
            },
            'itemTotal': {
                title: 'Total Price'
            }
            // totalAmount: {
            //     title: 'Total'
            // }
        },
        hideSubHeader: true
    };
    constructor(
        public _orderService: OrderService,
        private activatedRoute: ActivatedRoute,
        private _fb: FormBuilder,
        private router: Router
    ) {
        this.orderStatus = this._orderService.getOrderStatus();
        this.shipmentToForm = this._fb.group({
            'address': [{ value: '', disabled: true }],
            'unit_no': [{ value: '', disabled: true }],
            'postal_code': [{ value: '', disabled: true }],
            'contact_no': [{ value: '', disabled: true }],
            'recepient': [{ value: '', disabled: true }],
        });
        this.shipmentFromForm = this._fb.group({
            'agentName': [{ value: '', disabled: true }],
            'agentWeb': [{ value: '', disabled: true }],
            'agentContact': [{ value: '', disabled: true }],
            'buyerName': [{ value: '', disabled: true }],
            'fromAddress': [{ value: '', disabled: true }],
            'fromBld': [{ value: '', disabled: true }],
            'fromPostcode': [{ value: '', disabled: true }],
            'fromName': [{ value: '', disabled: true }],

            'fromContact': [{ value: '', disabled: true }],
            'fromEmail': [{ value: '', disabled: true }],
        });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (!params) { return; }
            this.order_id = params['orderid'];
            this._orderService.getOrderDetail(params['orderid']).subscribe(
                (data: any) => {
                    this.order = data;
                    const {
                        buyer_id,
                        memo,
                        commission_status,
                        status,
                        delivery,
                        delivery: { free_shipping: freeShipping, shipping_fee: shippingFee, shipment_id },
                        total: {
                            store_ap,
                            commission,
                            store_bp
                        } } = data;
                    this.status = status;
                    this.buyer = buyer_id;
                    this.shipmentToForm.patchValue(delivery);
                    this.orderItems = this.linearizeNestedArray(data);
                    this.source.load(this.linearizeNestedArray(data));
                    this.commissionStatus = commission_status;
                    this.orderDetail['memo'] = memo;
                    this.shipment_id = shipment_id || '';
                    this.orderDetail['storeTotal'] = store_bp;
                    this.orderDetail['totalShipment'] = data['free_shipping'] ? 0 : shippingFee;
                    this.orderDetail['discountAm'] = store_bp - store_ap;
                    this.orderDetail['totalPayable'] = store_ap + (data['free_shipping'] ? 0 : shippingFee);
                    this.orderDetail['gstockComm'] = commission.toFixed(2);
                    console.log('Order detail', this.order);
                },
                err => errorHandler(err)
            );
            this._orderService.getExpressAgent('buddypost').subscribe(
                onSuccess => {
                    this.shipmentAgent = onSuccess['value'].split(',');
                    this.shipmentFromForm.patchValue({
                        'agentName': this.shipmentAgent[0],
                        'agentWeb': this.shipmentAgent[1],
                        'agentContact': this.shipmentAgent[2]
                    });
                },
                onError => {
                    errorHandler('Express Error', onError);
                }
            );
            this._orderService.getMerchantProfile().subscribe(
                (payload: any) => {
                    const {
                        location: {
                            addressline1 = '',
                            postal_code = ''
                        },
                        profile: {
                            first_name = ''
                        },
                        contact: {
                            mobile_no = ''
                        }
                    } = payload;
                    this.shipmentFromForm.patchValue({
                        'fromAddress': addressline1,
                        'fromPostcode': postal_code,
                        'fromName': first_name,
                        'fromContact': mobile_no
                    });

                },
                onError => errorHandler(onError)
            );
        });

        this.getShipment();
    }
    private getComm() {

    }
    private getShipment() {
        this.shipmentData = this._orderService.getShipmentData();
    }
    private generateInvoice() {

        const doc = new jsPDF();
        doc.text(20, 20, 'From ' + 'Tony Store');
        doc.text(20, 30, 'No.');
        doc.text(150, 20, 'Sales Receipt');
        doc.text(20, 50, 'SOLD TO:');
        doc.text(150, 50, 'SHIP TO');
        doc.rect(30, 90, 150, 90);
        doc.line(30, 100, 180, 100);
        doc.line(30 + 37.5, 90, 30 + 37.5, 180);
        doc.line(30 + 37.5 * 2, 90, 30 + 37.5 * 2, 180);
        doc.line(30 + 37.5 * 3, 90, 30 + 37.5 * 3, 180);
        doc.text(33, 97, 'Description');
        doc.text(33 + 37.5 * 1, 97, 'Quantity');
        doc.text(33 + 37.5 * 2, 97, 'Unit Price');
        doc.text(33 + 37.5 * 3, 97, 'Amount');
        doc.text(150, 240, 'Total Amount');
        this.orderItems.forEach(
            (p, i) => {
                console.log(p);
                doc.text(33, 97 + 10 * (i + 1), p.name);
                doc.text(33 + 37.5 * 1, 97 + 10 * (i + 1), p.qty + '');
                doc.text(33 + 37.5 * 2, 97 + 10 * (i + 1), p.price + '');
                doc.text(33 + 37.5 * 3, 97 + 10 * (i + 1), p.qty * p.price + '');
                doc.text(150, 240, 'Total Amount');
                doc.line(30, 100 + 10 * (i + 1), 180, 100 + 10 * (i + 1));
            }
        );

        // Save the PDF
        doc.save('Test.pdf');

    }
    private statusChange() {
        this.shipment_id = '';
    }
    private linearizeNestedArray(obj) {
        const tmpArray = [];
        obj.products.forEach(product => {
            if (typeof product.variants[0]._id === 'undefined') {
                tmpArray.push({
                    'name': product.product.brief.name,
                    'option_value': 'Base Product',
                    'price': product.product.brief.price,
                    'discount_price': product.product.pricing.discount_price,
                    'qty': product.variants[0].order_qty,
                    'itemTotal': product.purchase.product_total_bp

                });
            } else {
                product.variants.forEach(v => {
                    const discountRate: number = product.product.pricing.discount_price / product.product.brief.price;
                    const discountPrice: number = v.price * discountRate;
                    tmpArray.push({
                        'name': product.product.brief.name,
                        'itemTotal': discountPrice < v.price ? v.order_qty * discountPrice : v.order_qty * v.price,
                        'qty': v.order_qty,
                        'price': v.price,
                        'discount_price': discountPrice,
                        'option_value': v.option_value
                    });
                });


                // product.variant.forEach(variant=>{
                //     tmpArray.push(
                //         {
                //             'name': product.product.brief.name,
                //             'itemTotal':product.purchase.product_total,
                //             'qty':product.purchase.qty,
                //             ...flatten()(variant)
                //         }

                //     )
                // }
                // )
            }
        });
        this.orderDetail['totalItem'] = tmpArray.length;
        return tmpArray;
    }

    // private updateShipment()
    // {

    // }

    private confirmDelivery(status) {
        if (!confirm(`   By clicking confirm order, your order will be submitted to delivery processing.You will be penalized if you fail to prepare order on collection date.`)) { return; }
        this._orderService.confirmOrder({ order_id: this.order_id, status: status }).subscribe(
            onSuccess => {
                console.log('Success', onSuccess);
                this.router.navigate(["/pages/order/order-overview"])
            },
            onError => {
                errorHandler(onError);
            }
        );

    }
    // private editShipmentF(action)
    // {
    //     if(action=="edit")
    //     {
    //     this.editShipment=true;
    //     console.log("Edit");
    //     }
    //     if(action=="cancel")
    //     {
    //         this.editShipment=false;

    //     }


    // }
}
