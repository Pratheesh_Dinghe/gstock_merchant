
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService } from '../../shared/auth/auth.service';


@Injectable()
export class OrderService {
  private url = environment.apiUrl;
  public authHeader;

  constructor(private httpClient: HttpClient, private _authService: AuthService) {
    this.authHeader = this._authService.getAuthHeader();
  }
  public getOrderSummary(orderId) {
    const orderSummary = {
      seller: 'Tony-Tony',
      buyer: 'David',
      orderDate: '23/09/2017 23:23:44',
      orderStatus: 'as'
    };
    return orderSummary;
  }

  public generateInvoice() {
    return this.httpClient.get(this.url + '/merchant/order/invoice', { headers: this.authHeader, responseType: 'arraybuffer' });
  }

  public searchUsersByUserId(body) {
    return this.httpClient.post(this.url + '/admin/searchusersbyuserid', body);

  }
  public getExpressAgent(name) {
    return this.httpClient.get(this.url + '/merchant/express?provider=' + name, { headers: this.authHeader });
  }

  public updateOrder(body) {
    return this.httpClient.put(this.url + '/admin/orders/status', body, { headers: this.authHeader });
  }
  public getMerchantProfile() {
    return this.httpClient.get(this.url + '/user/account/detail?select=profile/company/contact/location', { headers: this.authHeader });

  }

  public updateBuyerAddress(body) {

  }
  public confirmOrder(body) {

    return this.httpClient.post(this.url + '/merchant/order/confirm', body, { headers: this.authHeader });
  }

  public getOrders(query = '') {

    return this.httpClient.get(`${this.url}/merchant/order${query}`, { headers: this.authHeader });
  }

  public getOrderDetail(id) {
    return this.httpClient.get(this.url + '/merchant/order?order_id=' + id, { headers: this.authHeader });
  }
  public getShipmentData() {
    const shipmentData = {
      from: {
        name: 'Tony&Tony',
        address: 'Fajar Road',
        contact: '23423424',
        building: 'BLK440',
        postal: '323423',
        unit: '09-34',
        email: 'xianeieo@gmail.com'
      },
      to: {
        name: 'David',
        address: 'Bukit Batok',
        contact: '3234342',
        building: 'BLk34',
        postal: '2323',
        unit: '09-22',
        email: 'dddd@gmail.com'
      },
      shipmentStatus: 'Completed',
      agent: 'Buddypost'
    };
    return shipmentData;
  }

  public getOrderStatus() {

    const orderStatus = [
      {
        name: 'Awaiting Delivery',
        value: 'AD'
      },
      {
        name: 'Delivering',
        value: 'DG'
      },
    ];

    return orderStatus;
  }
}
