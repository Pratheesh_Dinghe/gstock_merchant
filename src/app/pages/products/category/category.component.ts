import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation } from '@angular/core';

import { CategoryService } from './category.service';

@Component({
  selector: 'az-category',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent {

  private triplePanel = [];
  private rawCatList = [];
  private selected = [];
  private settings = {
    add: {
      confirmCreate: true,
    },
    delete: {
      confirmDelete: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Cateogry Name'
      },
    },
  }

  constructor(
    public _categoryService: CategoryService
  ) {
    this.loadTable((data) => {
      this.rawCatList = data;
      this.triplePanel = this.select('', -1)
    })
  }
  private loadTable(cb?) {
    /* Load source and perform a callback once loaded */
    return this._categoryService.getAllCategory().subscribe(data => {
      cb(data)
    })
  }

  private expand(original, main, level, accumulator) {
    /* Recursively open up subcategory branch to last level
     * Return subcategory array and the path
     */
    if (level === 2) { return { 'sub': accumulator, 'path': main.split(',') } }
    const sub = CategoryService.subCat(original, main, level)
    accumulator.push(sub.map((cat, index) => {
      return {
        'name': cat,
      }
    }))
    main += main === '' ? sub[0] : (',' + sub[0])

    return this.expand(original, main, level + 1, accumulator)
  }
  private select(name, level) {
    /* Open up subcategory branch by name and level
     * Refresh selected path and panel
     */
    const { sub, path } = this.expand(this.rawCatList, name, level, []);
    level <= 0 ? this.selected = path : this.selected[level] = name
    console.log(sub)
    this.triplePanel.forEach((e, i) => {
      if (i <= level) { return }
      this.triplePanel[i] = sub[i - level - 1] || []
    })
    return sub
  }
  private onCreateConfirm(event, level) {
    const newName = event.newData.name
    if (level === 0) {
      return this._categoryService.postCreateMainCat({ name: ',' + newName })
        .subscribe((message) => {
          alert(message)
          console.log(message)
          this.loadTable((data) => {
            this.rawCatList = data
            event.confirm.resolve()
          })
        },
          (err) => {

            return errorHandler(err)
          })
    }
    const oldName = ',' + this.selected.splice(0, level).join(',')
    return this._categoryService.postAddSubCat({
      parent: oldName,
      child: newName
    })
      .subscribe((message) => {
        alert(message)
        console.log(message)
        this.loadTable((data) => {
          this.rawCatList = data
          event.confirm.resolve()
          this.selected[level] = newName
        })

      },
        (err) => {
          return errorHandler(err)
        })
  }
  private onEditConfirm(event, level) {
    const oldName = event.data.name
    const newName = event.newData.name
    return this._categoryService.putUpdateSubCat({
      old: oldName,
      new: newName
    }).subscribe(
      () => {
        this.loadTable((data) => {
          this.rawCatList = data
          this.selected[level] = newName
          event.confirm.resolve()
        })
      },
      (err) => { return });
  }
  private onDeleteConfirm(event, level) {
    return this._categoryService.deleteRemoveSubCat({
      category: event.data.name
    }).subscribe(
      (message) => {
        alert(message)
        this.loadTable((data) => {
          this.rawCatList = data
          this.selected[level] = ''
          event.confirm.resolve()

        })
        event.confirm.resolve()
      },
      (err) => { return errorHandler(err) });
  }
}
