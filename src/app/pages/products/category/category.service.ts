import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';




@Injectable()
export class CategoryService {
    public static subCat(original: Array<any>, parent: string, offset: number) {
        /* Retrive data.path array and transform to array of category by level
         * Root categories array start at level -1
         */
        const myReg = new RegExp('^,' + parent + '.+?,', 'i');
        const subCategory = original
            .filter((filter) => {
                if (parent) { return myReg.exec(filter.path) !== null ? true : false; }
                return filter;
            })
            /* Transfrom into array e.g. => ['','Food','Baby','Milk'] =>['Food','Baby','Milk']
             * Slice to get sub category ['Food','Baby','Milk'].slice(1+ offset) => Food
             */
            .map((split) => {
                const path = split.path.split(',').slice(1 + offset);
                return path[0];
            });
        return Array.from(new Set(subCategory)).map((e: string) => {
            const index = original.findIndex((self) => {
                if (parent !== '') { return self.path === ',' + parent + ',' + e + ',' ? true : false; }
                return self.path === parent + ',' + e + ',' ? true : false;
            });
            return {
                name: e,
                commission: index > 0 ? original[index].commission : '没有'
            };
        });
    }


    private url = environment.apiUrl;
    private headers = new Headers({ 'Content-Type': 'application/json', 'authorization': 'JWT ' + window.sessionStorage.accessToken });

    constructor(private http: HttpClient) { }

    postCreateMainCat(body) {
        return this.http.post(this.url + '/product/category', body, { responseType: 'text' });
    }
    getAllCategory() {
        return this.http.get(this.url + '/product/category', { responseType: 'json' });
    }

    getOneCategory(path) {
        return this.http.get(this.url + '/product/category?path='+path, { responseType: 'json' });
    }
    putUpdateSubCat(body) {
        return this.http.put(this.url + '/product/category', body, { responseType: 'text' });
    }
    deleteRemoveSubCat(body) {
        return this.http.post(this.url + '/product/category/delete', body, { responseType: 'text' });
    }
    postAddSubCat(body) {
        return this.http.post(this.url + '/product/category/append', body, { responseType: 'text' });
    }
    putUpdateCommission(body) {
        return this.http.put(this.url + '/product/category/commission', body, { responseType: 'text' });
    }
}
