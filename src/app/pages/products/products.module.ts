import { PaginationComponent } from "../../theme/util-components/pagination/pagination.component";
import { CategoryService } from "./category/category.service";
import { AgmCoreModule } from "@agm/core";
import { CommonModule, DecimalPipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
// import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";
import { DragulaModule } from "ng2-dragula";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { MultiselectDropdownModule } from "angular-2-dropdown-multiselect";
import { DirectivesModule } from "../../theme/directives/directives.module";
import { CategoryComponent } from "./category/category.component";
import { ButtonViewComponent } from "./product-list/button-view.component";
import { ButtonDetailComponent } from "./product-list/button-detail.component";
import { ButtonApproveComponent } from "./product-list/button-approve.component";
import { ButtonAssignComponent } from "./product-detail/button-assign/button-assign.component";

import { ProductListComponent } from "./product-list/product-list.component";
import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { ProductNewComponent } from "./product-new/product-new.component";
import { ProductService } from "./products.service";
import { environment } from "../../../environments/environment";
import { ImageModalComponent } from "./product-new/image-modal/image-modal.component";
import { DropzoneModule } from "ngx-dropzone-wrapper";
import { ComponentModule } from "../../_component/component.module";
import { UtilComponentsModule } from "../../theme/util-components/util.module";
const picCount = 0;
export const routes = [
  { path: "", redirectTo: "product-list", pathMatch: "full" },
  {
    path: "product-list",
    component: ProductListComponent,
    data: { breadcrumb: "Products List" }
  },
  {
    path: "product-new",
    component: ProductNewComponent,
    data: { breadcrumb: "New Product" }
  },
  {
    path: "product-detail/:id",
    component: ProductDetailComponent,
    data: { breadcrumb: "Products Detail" }
  },
  {
    path: "category",
    component: CategoryComponent,
    data: { breadcrumb: "Products Categories" }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    RouterModule.forChild(routes),
    DragulaModule,
    Ng2SmartTableModule,
    MultiselectDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    DropzoneModule,
    ComponentModule,
    UtilComponentsModule
  ],
  declarations: [
    ProductListComponent,
    CategoryComponent,
    ProductDetailComponent,
    ProductNewComponent,
    ButtonApproveComponent,
    ButtonAssignComponent,
    ButtonViewComponent,
    ButtonDetailComponent,
    ImageModalComponent,
    PaginationComponent
  ],
  providers: [ProductService, CategoryService, DecimalPipe]
})
export class ProductsModule {}
