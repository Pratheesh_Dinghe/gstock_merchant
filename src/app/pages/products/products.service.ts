import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { AuthService } from "../../shared/auth/auth.service";

@Injectable()
export class ProductService {
  private url = environment.apiUrl;
  // private headers = new HttpHeaders({ 'authorization': 'JWT ' + window.sessionStorage.accessToken });
  public authHeader;

  constructor(private http: HttpClient, private _authService: AuthService) {
    this.authHeader = _authService.getAuthHeader();
  }

  // public searchUsersByUserId(body) {
  //   return this.http.post(this.url + '/admin/searchusersbyuserid', body, { headers: this.authHeader });

  // }

  // public getCategory(cate, level) {
  //   return this.http.get(this.url + '/product_category/cat/' + cate + '/level/' + level, { headers: this.authHeader });
  // }

  public postMainCate(body) {
    return this.http.post(this.url + "/product_category/" + body, {
      headers: this.authHeader
    });
  }

  public getProduct(id) {
    return this.http.get(this.url + "/merchant/product?page=1&limit=10");
  }
  public deleteVariant(body) {
    return this.http.post(this.url + "/merchant/variant/delete", body, {
      headers: this.authHeader
    });
  }
  public uploadVariant(body) {
    return this.http.post(this.url + "/merchant/variant", body, {
      headers: this.authHeader
    });
  }

  public getOneCategory(path) {
    return this.http.get(
      this.url + "/product/category?path=" + encodeURIComponent(path)
    );
  }
  public updateAttribute(body) {
    return this.http.put(this.url + "/merchant/product", body, {
      headers: this.authHeader
    });
  }

  public assignVariantPic(body) {
    return this.http.put(this.url + "/merchant/variant", body, {
      headers: this.authHeader
    });
  }

  public updateVariant(body) {
    const headers = new HttpHeaders(this.authHeader);
    return this.http.put(this.url + "/merchant/variant", body, {
      headers: headers
    });
  }

  public getProductDetail(query) {
    return this.http.get(`${this.url}/merchant/product?${query}`, {
      headers: this.authHeader
    });
  }
  public getProductVariant(id) {
    return this.http.get(this.url + "/merchant/variant?product_id=" + id, {
      headers: this.authHeader
    });
  }
  public updateProduct(body) {
    return this.http.post(this.url + "/merchant/product/", body, {
      headers: this.authHeader
    });
  }

  public deleteProduct(body) {
    return this.http.post(`${this.url}/merchant/product/bulk/delete`, body, {
      headers: this.authHeader
    });
  }
  public searchProduct(query) {
    // alert(this.url + "/merchant/product" + query);
    return this.http.get(this.url + "/merchant/product" + query, {
      headers: this.authHeader
    });
  }
  public changeProductActiveStatus(query) {
    return this.http.put(
      `${this.url}/merchant/product/active?${query}`,
      {},
      { headers: this.authHeader, responseType: "text" }
    );
  }
}
