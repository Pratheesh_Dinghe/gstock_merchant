import { errorHandler } from './../../../_helper/errorHandler';
import { CategoryService } from '../category/category.service';
import { DatePipe, DecimalPipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DragulaService } from 'ng2-dragula/components/dragula.provider';
import { LocalDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';
import { ProductService } from '../products.service';
import { NewProductService } from './product-new.service';

import { ImageModalComponent } from './image-modal/image-modal.component';
import { AuthService } from '../../../shared/auth/auth.service';
import { CustomValidatorsService } from '../../../shared/validators/custom-validator.service';
import { dropzoneConfig } from '../../../_global/dropzone.config';
import { froalaConfig } from '../../../_global/froala.config';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'az-product-new',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './product-new.component.html',
    styleUrls: ['./product-new.component.scss'],
    providers: [NewProductService]
})

export class ProductNewComponent {
    // @ViewChild('imageModal') imageModal: ImageModalComponent;
    private product;
    private picCount = 0;
    private product_id = '';
    public editorOptions;
    // Wizard
    public steps: any[];
    public allowBack = false;
    private productBasicForm: FormGroup;
    private productDetailForm: FormGroup;
    private attributeForm: FormGroup;
    private variantForm: FormGroup;
    private dropzoneConfig = {};
    private cateAttribute = [];
    private variantOptions = [];
    private variantArray = [];
    private cateId;
    private mainCategory = [];
    private subCategory = [];
    private subSubCategory = [];
    public categorySelected;
    private categoryPath = [];
    private allCategory = [];
    /*
     */
    private variantImages = [];
    private source: LocalDataSource;
    private settings = {
        actions: {
            add: false,
        },
        edit: {
            confirmSave: true
        },
        columns: {
            option_name: {
                title: 'Option',
                editable: false
            },
            option_value: {
                title: 'Value',
                editable: false
            },
            price: {
                title: 'Variant Price',
            },
            discount_rate: {
                title: 'Discount Rate',
                editable: false
            },
            discount_price: {
                title: 'Discounted Price',
            },
            stock: {
                title: 'Qty',
            },
            sku: {
                title: 'SKU',
                editable: true
            }
        }
    };
    private config = {
        variants: [],
        source: undefined,
        default: undefined
    };
    constructor(
        private _productService: ProductService,
        private _categoryService: CategoryService,
        private _newProductService: NewProductService,
        private _dragulaService: DragulaService,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _toastr: ToastrService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private changeDetectorRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer,

    ) {
        // Wizardz
        this.steps = [
            { name: 'Product Information', icon: 'fa fa-pencil', active: true, valid: false, hasError: false },
            { name: 'Product Detail', icon: 'fa fa-suitcase', active: false, valid: false, hasError: false },
            { name: 'Product Picture', icon: 'fa fa-file-photo-o', active: false, valid: false, hasError: false },
            { name: 'Product Variant', icon: 'fa fa-copy', active: false, valid: false, hasError: false }
        ];
        this.allowBack = false;
        this.productBasicForm = this.formBuilder.group(
            {
                mainCategory: ['', Validators.required],
                subCategory: ['', Validators.required],
                subSubCategory: ['', Validators.required],
                cateCom: ['']
            }
        );
        this.productDetailForm = this.formBuilder.group(
            {
                name: ['', [Validators.required, Validators.maxLength(50)]],
                sku: ['', [Validators.required, Validators.maxLength(15)]],
                discount: [false],
                barcode: ['', Validators.maxLength(15)],
                long_description: [''],
                brand: ['', [Validators.required, Validators.maxLength(15)]],
                price: ['', [Validators.required, CustomValidatorsService.dollar(), CustomValidatorsService.min(0.1)]],
                discount_rate: [''],
                discount_price: [''],
                stock: [100, [Validators.required, CustomValidatorsService.integer(), CustomValidatorsService.min(1)]],
            }
        );
        const {
            discount: discountController,
            discount_rate: discountRateController
        } = this.productDetailForm.controls;
        discountController.valueChanges.subscribe((discount) => {
            discountRateController.setValidators(discount ? CustomValidatorsService.range(0, 100, false) : null);
            discountRateController.updateValueAndValidity();
        });

        this.source = new LocalDataSource();
        this.config.source = this.source;
        // this.productDetailForm.value.mainCategory = 'null';
        this.getAllCate();
    }
    /**
     * Step One
     */
    private getAllCate() {
        return this._categoryService.getAllCategory().subscribe(
            (category: any) => {
                this.productBasicForm.controls['mainCategory'].valueChanges.subscribe(
                    value => {
                        this.cateAttribute = [];
                        this.categoryPath[0] = value;
                        this.categoryPath = this.categoryPath.slice(0, 1);
                        this.subCategory = CategoryService.subCat(this.allCategory, this.categoryPath.join(','), 1)
                            .map(cat => cat.name);
                        this.productBasicForm.controls['subCategory'].setValue('', { emitEvent: false });
                        this.productBasicForm.controls['subSubCategory'].setValue('', { emitEvent: false });
                    }
                );
                this.productBasicForm.controls['subCategory'].valueChanges.subscribe(
                    value => {
                        this.cateAttribute = [];
                        this.categoryPath[1] = value;
                        this.categoryPath = this.categoryPath.slice(0, 2);
                        this.subSubCategory = CategoryService.subCat(this.allCategory, this.categoryPath.join(',') + ',', 2)
                            .map(cat => cat.name);
                        this.productBasicForm.controls['subSubCategory'].setValue('', { emitEvent: false });
                    }
                );
                this.productBasicForm.controls['subSubCategory'].valueChanges.subscribe(
                    value => {
                        this.cateAttribute = [];
                        this.categoryPath[2] = value;
                    }
                );
                this.allCategory = category;
                this.mainCategory = CategoryService.subCat(this.allCategory, '', 0).map(cat => cat.name);
            }, (onError) => {
                this._toastr.error('Server Error, Please refresh and try again');
                errorHandler(onError);
            }
        );
    }
    private cateChange() {
        const matched = false;
        const { mainCategory, subCategory, subSubCategory } = this.productBasicForm.value;
        this.categorySelected = `,${mainCategory},${subCategory},${subSubCategory},`;
        const cate = this.allCategory.find((c) => c.path === this.categorySelected);
        this.productBasicForm.patchValue({ cateCom: cate ? cate.commission : 'unknown' });
        const { _id, commission, attributes } = cate;
        const commList = this._authService.getCommissionList();
        const specialComm = commList.find((c) => c.category_id === _id);
        if (specialComm) {
            this.productBasicForm.patchValue({ cateCom: specialComm.rate });
        }
        if (cate.attributes) {
            this.cateAttribute = cate.attributes.filter(a => !a.variant);
            this.attributeForm = this.generateAttributeForm(this.cateAttribute);
            this.config.variants = cate.attributes.filter(a => a.variant);
        }
        return cate._id;
    }
    private transformAttributeSelected(value) {
        const r = Object.keys(value).map(
            key => {
                return {
                    'name': key,
                    'value': value[key]
                };
            });
        return r;
    }
    private generateAttributeForm(attributes) {
        const formGroup = {};
        attributes.forEach(
            a => formGroup[a.name] = new FormControl(
                a.value[0]
            )
        );
        return new FormGroup(formGroup);
    }
    /**
     * Step Two
     */
    public productInformation() {
        const attributes = this.attributeForm ? this.transformAttributeSelected(this.attributeForm.value) : [];
        const basic = {
            category_id: this.cateId,
            attributes,
            category: this.categorySelected
        };
        return this._newProductService.uploadBasicProduct(basic).map(
            onSuccess => {
                this.product_id = onSuccess['product_id'];
                const header = this._authService.getAuthUploadHeader();
                console.log("header", header)
                this.editorOptions = {
                    ...froalaConfig,
                    imageUploadURL: `${environment.apiUrl}/merchant/thumbnail/description?product_id=${this.product_id}`,
                    requestHeaders: header
                };
                this.dropzoneConfig = Object.assign(dropzoneConfig, {
                    url: `${environment.apiUrl}/merchant/thumbnail/slider/${this.product_id}`,
                    headers: header,
                });
            }
        );
    }
    /**
     * Step Three
     */
    private onUploadError($event) {
        this.picCount = 0;
        console.log('File', $event);
    }
    private onAddedPic($event) {
        console.log('Added', $event);
    }
    private removePic(file) {
        // const filename = file.upload.filename + '.png';
        // const filename2 = {
        //     'remove': filename
        // };
        // this._newProductService.deletePhoto(filename2, this.product_id).subscribe(
        //     onSuccess => {
        //         this._toastr.success('Photo removed');
        //     },
        //     onError => {
        //         this._toastr.error('Photo remove failed');
        //     }
        // );

    }
    /**
     * Step Four
     */
    private async addVariants(newVariants) {
        console.log('product-new.addVariant', newVariants);

        const price = this.productDetailForm.value.price;
        if (newVariants[0].price <= price * 0.5 || newVariants[0].price >= price * 1.5) {
            console.log('new variant having price out of range');
            this._toastr.error('Please set to between -50% and +100% of product listing price.');
            return;
        }

        const discountRate = this.productDetailForm.value.discount_rate || 0;
        const newVariantsAfterAppendDiscountPrice = newVariants.map(v => (
            {
                ...v,
                discount_rate: discountRate,
                discount_price: v.price * (1 - (discountRate / 100))
            }
        ));
        const all = await this.source.getAll();
        await this.source.load([...all, ...newVariantsAfterAppendDiscountPrice]);
    }
    private async select(event) {
        // event.data['images'] = await this.loadImage(event);
        // this.selected = event;
        return;
    }

    private onEditVariantConfirm(event) {
        console.log(event);
        const price = this.productDetailForm.controls.price.value;
        console.log(`price: ${price * 0.5} < base(${price}) < ${price * 2}`);

        if (event.newData['price'] <= price * 0.5 || event.newData['price'] >= price * 2) {
            this._toastr.error('Please set to between -50% and +100% of product listing price.');
            event.confirm.reject();
        } else {
            event.confirm.resolve(event.newData);
        }
    }

    private clearVariants() {
        this.source.load([]);
    }
    /**
     * Flow
     */
    public next() {
        if (this.steps[this.steps.length - 1].active) { return false; }
        this.steps.some((step, index, steps) => {
            if (index < steps.length - 1 && step.active) {
                if (!this.productBasicForm.valid || (this.attributeForm ? !this.attributeForm.valid : false)) {
                    step.hasError = true;
                    this._toastr.error('Please fill in the category and attribute field');
                }
                switch (step.name) {
                    case 'Product Information': {
                        this.productInformation().subscribe(
                            a => {
                                this._toastr.success('Base product created!');
                                step.active = false;
                                step.valid = true;
                                steps[index + 1].active = true;
                            }, onError => {
                                this._toastr.error(onError);
                                step.active = true;
                                step.valid = false;
                                step.hasError = true;
                            }
                        );
                        break;
                    }
                    case 'Product Detail': {
                        this.allowBack = true;
                        if (!this.productDetailForm.valid) { step.hasError = true; break; }
                        const details = {
                            'product_id': this.product_id,
                            'detail': {
                                ...this.productDetailForm.value
                            },
                            'brief': {
                                ...this.productDetailForm.value
                            },
                            'stock': {
                                'qty': this.productDetailForm.value.stock
                            },
                            'pricing': {
                                'discount_rate': this.productDetailForm.value.discount_rate
                            }
                        };
                        this.config = {
                            ...this.config,
                            default: {
                                ...this.productDetailForm.value
                            }
                        };
                        this._newProductService.updateDetailProduct(details).subscribe(
                            onSuccess => {
                                this._toastr.success('Product detail update successful');
                                step.active = false;
                                step.valid = true;
                                steps[index + 1].active = true;
                                return true;
                            },
                            onError => {
                                this._toastr.error(onError);
                                step.active = true;
                                step.valid = false;
                                step.hasError = true;
                            }
                        );
                        break;
                    }
                    case 'Product Picture': {
                        step.active = false;
                        step.valid = true;
                        steps[index + 1].active = true;
                        break;
                    }
                    default: return false;
                }
                return true;
            }
        });
    }
    public prev() {
        if (this.steps[0].active || this.steps[1].active) { return false; }
        this.steps.some(function (step, index, steps) {
            if (index !== 0) {
                if (step.active) {
                    step.active = false;
                    steps[index - 1].active = true;
                    return true;
                }
            }
        });
    }
    public async confirm() {
        this.steps.forEach(step => step.valid = true);
        if (!this.config.variants.length) {
            this._toastr.success('Your product is created!');
            return this._router.navigate(['pages/products/product-list']);
        }
        const variant = {
            'variants': await this.source.getAll(),
            'product_id': this.product_id
        };
        this._newProductService.uploadVariant(variant).subscribe(
            onSuccess => {
                this._toastr.success('Variants created');
                this._router.navigate(['pages/products/product-list']);
            }, e => this._toastr.error(e.error.error + ' Please set to between -50% and +100% of product listing price.')
        );
    }
    public clearAll() {
        this.source.load([]);
    }
}

