import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthService } from '../../../shared/auth/auth.service';

@Injectable()
export class NewProductService {
    public url = environment.apiUrl;
    public authHeader;


    // public merchantUrl = environment.apiUrlForMerchant;
    constructor(private httpClient: HttpClient, private _authService: AuthService) {
        this.authHeader = this._authService.getAuthHeader();
    }
    public uploadImage(productId, body) {
        return this.httpClient.post(this.url + '/merchant/thumbnail/slider/' + productId, body);
    }
    public uploadVariantImage(productId, body) {
        return this.httpClient.post(this.url + '/merchant/thumbnail/slider/' + productId, body);
    }

    public uploadBasicProduct(body) {
        return this.httpClient.post(this.url + '/merchant/product', body, { headers: this.authHeader });
    }

    public getVariant(cateid) {
        return this.httpClient.get(this.url + '/merchant/variant_options?category_id=' + cateid, { headers: this.authHeader });
    }

    public uploadVariant(body) {
        return this.httpClient.post(this.url + '/merchant/variant', body, { headers: this.authHeader, responseType: 'text' });
    }



    public deletePhoto(body, productid) {
        return this.httpClient.post(this.url + '/merchant/thumbnail/delete/' + productid, body, { headers: this.authHeader });
    }

    public updateDetailProduct(body) {
        return this.httpClient.put(this.url + '/merchant/product/', body, { headers: this.authHeader });
    }

}
