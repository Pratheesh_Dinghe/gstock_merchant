import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Renderer2, Component, ViewChild, ElementRef, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-approve',
    templateUrl: './button-approve.component.html',
})
export class ButtonApproveComponent implements ViewCell {
    @ViewChild('status', { read: ElementRef }) status: ElementRef;
    @Input() value;
    @Input() rowData: any;
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private renderer: Renderer2,
        private el: ElementRef
    ) {

    }
    ngOnInit() {
        switch (this.value.status) {
            case 'Approved': {
                this.renderer.addClass(this.status.nativeElement, 'btn-success')
                break;
            }
            case 'Rejected': {
                this.renderer.addClass(this.status.nativeElement, 'btn-danger')
                break;
            }
            case 'Pending': {
                this.renderer.addClass(this.status.nativeElement, 'btn-info')
                break;
            }
        }
    }
}

