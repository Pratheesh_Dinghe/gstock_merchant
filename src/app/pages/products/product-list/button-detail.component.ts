import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewCell } from 'ng2-smart-table';

import { NewProductService } from '../product-new/product-new.service';
import { AuthService } from '../../../shared/auth/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'button-view',
    template: `
  <div style="position:relative; text-align: center;">
    <button class=" btn-rounded" (click)="selectProduct()">View</button>
  </div>
    `,
    providers: []
})

export class ButtonDetailComponent implements ViewCell, OnInit {
    @Input() value: string | number;
    @Input() rowData: any;
    constructor(
        private _authService: AuthService,
        private _router: Router
    ) { }
    ngOnInit() {

    }

    private selectProduct(): void {
        this._router.navigate(['pages/products/product-detail/' + this.rowData.id]);
    }
}

