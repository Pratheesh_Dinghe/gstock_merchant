import { PaginationComponent } from "../../../theme/util-components/pagination/pagination.component";
import { errorHandler } from "../../../_helper/errorHandler";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  Component,
  ViewChild,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import * as flatten from "flatten-obj";
import { LocalDataSource } from "ng2-smart-table";
import { DatePipe } from "@angular/common";
import { ProductService } from "../products.service";
import { ButtonViewComponent } from "./button-view.component";
import { ButtonApproveComponent } from "./button-approve.component";
import { ButtonDetailComponent } from "./button-detail.component";
import { LoaderService } from "../../../_component/loader/loader.service";
import { ToastrService } from "ngx-toastr";
import { HttpErrorResponse } from "@angular/common/http";
// import { Date} from 'core-js/library/web/timers';

@Component({
  selector: "az-product-list",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.scss"],
  entryComponents: [
    ButtonViewComponent,
    ButtonApproveComponent,
    ButtonDetailComponent
  ]
})
export class ProductListComponent implements OnInit {
  @ViewChild("myModal") myModal: ElementRef;
  @ViewChild("paginator") paginationComponent: PaginationComponent;
  private searchForm: FormGroup;
  private query = "";
  private source = new LocalDataSource();
  private origin = [];
  private startDate;
  private endDate;
  private statusChangeEmitter: EventEmitter<string>;
  private productIdSelected: string;
  private settings = {
    selectMode: "multi",
    hideSubHeader: true,
    actions: false,
    columns: {
      "brief.image": {
        filter: false,
        title: "Picture",
        type: "custom",
        renderComponent: ButtonViewComponent
      },
      "brief.name": {
        title: "Product Name",
        valuePrepareFunction: name => (name ? name : "**Incomplete**")
      },
      "stock.qty": {
        title: "Quantity"
      },
      status: {
        title: "Status",
        type: "custom",
        renderComponent: ButtonApproveComponent,
        valuePrepareFunction: (cell, row) => {
          return {
            _id: row._id,
            status: row.status
          };
        }
      },
      createdAt: {
        title: "Create Date",
        valuePrepareFunction: date => {
          const raw = new Date(date);
          const formatted = new DatePipe("en-EN").transform(
            raw,
            "dd-MM-yyyy HH:mm"
          );
          return formatted;
        }
      },
      detail: {
        title: "Detail",
        filter: false,
        type: "custom",
        renderComponent: ButtonDetailComponent
      }
    }
  };

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _productService: ProductService,
    private _loader: LoaderService,
    private fb: FormBuilder,
    private _toast: ToastrService
  ) {}
  ngOnInit() {
    this.query = "&name=";
    this.formInit();
    this.toPage(1);
  }
  private formInit() {
    this.searchForm = this.fb.group({
      name: [],
      start: [],
      end: [],
      status: []
    });
  }
  private statusChange(status) {
    this.searchForm.value["status"] = status;
    this.searchProduct();
  }
  private dateChange(date) {
    // var utc= new DatePipe('en-EN').transform(date.value,"yyyy-MM-dd HH:mm:ss",);
    // console.log("UTC",utc);
    const localTime = new Date(date.value);
    const utcDate = new Date(
      localTime.getUTCFullYear(),
      localTime.getUTCMonth(),
      localTime.getUTCDate(),
      localTime.getUTCHours(),
      localTime.getUTCMinutes(),
      localTime.getUTCSeconds()
    );
    const utcDate2 = new DatePipe("en-EN").transform(
      utcDate,
      "dd-MM-yyyy HH:mm"
    );
    // var utcDate = new Date(date.value.getUTCFullYear(), date.value.getUTCMonth(), date.value.getUTCDate(),  date.value.getUTCHours(), date.value.getUTCMinutes(), date.value.getUTCSeconds());

    // console.log("Changed",new DatePipe('en-EN').transform(utcDate, 'dd-MM-yyyy HH:mm'));
  }

  private dateChange2(date) {
    const utc = new DatePipe("en-EN").transform(
      date.value,
      "yyyy-MM-dd HH:mm:ss -0800"
    );
    this.endDate = utc;
    // console.log("UTC2",this.startDate);
    // var utcDate = new Date(date.value.getUTCFullYear(), date.value.getUTCMonth(), date.value.getUTCDate(),  date.value.getUTCHours(), date.value.getUTCMinutes(), date.value.getUTCSeconds());

    // console.log("Changed",new DatePipe('en-EN').transform(utcDate, 'dd-MM-yyyy HH:mm'));
  }

  private async multiSelect(data) {
    this.productIdSelected = data.selected.map(e => e._id);
  }
  // private selectProduct(e): void {
  //     this._router.navigate(['pages/products/product-detail/' + e.data._id]);
  // }
  private searchProduct() {
    const form = this.searchForm.value;
    this.query = Object.keys(form)
      .map(field => (form[field] ? `${field}=${form[field].trim()}` : false))
      .filter(e => e)
      .join("&");
    console.log(this.query);
    this.toPage(1);
    // const nameQuery = '&name=' + encodeURIComponent(this.searchForm.value['name'] || '');
    // // + ('&intervals=' + this.startDate + '/' + this.endDate || '')
    // this.query = nameQuery;
    // this.paginationComponent.inited = false;
    // this.toPage(1);
  }

  private toPage(number) {
    this._loader.show();
    const body = `?page=${number}&limit=10&select=detail/brief/stock/createdAt/status&${
      this.query
    }`;
    return this._productService.searchProduct(body).subscribe(
      data => {
        console.log(data["docs"]);
        this.paginationComponent.init(
          Math.ceil(parseInt(data["total"], 10) / parseInt(data["limit"], 10))
        );
        this.source.load(
          data["docs"].map(product => {
            this.origin = data["docs"];
            return flatten()(product);
          })
        );
        console.log(this.source);
        this._loader.hide();
      },
      err => {
        this._loader.hide();
        errorHandler(err);
      }
    );
  }


  
  private deleteProducts() {
    this._productService
      .deleteProduct({ product: this.productIdSelected })
      .subscribe(
        (s: any) => {
          this._toast.success(s.message);
          alert(s.message);
          this.searchProduct();
        },
        (e: HttpErrorResponse) => {
          this._toast.error(e.error.message);
        }
      );
  }
}
