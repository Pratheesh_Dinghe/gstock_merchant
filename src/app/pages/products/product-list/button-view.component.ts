import { environment } from "../../../../environments/environment";
import { errorHandler } from "../../../_helper/errorHandler";
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ViewCell } from "ng2-smart-table";

import { NewProductService } from "../product-new/product-new.service";
import { AuthService } from "../../../shared/auth/auth.service";

@Component({
  selector: "button-view",
  template: `
    <div style="position:relative; ">
      <img style="max-width:60px;" [attr.src]="image" />
    </div>
  `,
  providers: []
})
export class ButtonViewComponent implements ViewCell, OnInit {
  private image;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(private _authService: AuthService) {}
  ngOnInit() {
    const { "brief.images": images = [], _id: productId } = this.rowData;

    if (!images.length) {
      return;
    }
    this.image = `${
      environment.apiUrl
    }/uploads/user/${this._authService.getUser()}/products/${productId}/${
      images[0]
    }`;
  }
}
