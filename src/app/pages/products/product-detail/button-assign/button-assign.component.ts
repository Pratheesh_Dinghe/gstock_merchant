import { environment } from '../../../../../environments/environment';
import { errorHandler } from '../../../../_helper/errorHandler';
import { Component, ViewChild, ElementRef, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { addTimestamp } from '../../../../_helper/addTimestamp';

@Component({
    selector: 'button-assign',
    templateUrl: './button-assign.component.html',
    styleUrls: ['./button-assign.component.scss'],
})
export class ButtonAssignComponent implements ViewCell {
    @Input() value;
    @Output() save: EventEmitter<any> = new EventEmitter();
    public thumbnailUrl = '';
    public _rowData;
    set rowData(rowData) {
        this._rowData = rowData;
        this.thumbnailUrl = rowData.thumbnailUrl;
        this._change.detectChanges();
    }


    constructor(
        private _change: ChangeDetectorRef
    ) {

    }
    ngOnInit() {

    }
    onClick() {
        console.log(this._rowData);
        this.save.emit(this._rowData);
    }
}

