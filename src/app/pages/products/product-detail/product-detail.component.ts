// import { this._toastr.error } from './../../../_helper/this._toastr.error';
import { CategoryService } from '../category/category.service';
import { DatePipe, Location, DecimalPipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DragulaService } from 'ng2-dragula/components/dragula.provider';
import { LocalDataSource } from 'ng2-smart-table';
import { ButtonAssignComponent } from './button-assign/button-assign.component';

import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';
import { ProductService } from '../products.service';
import { NewProductService } from '../product-new/product-new.service';
import { AuthService } from '../../../shared/auth/auth.service';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { CustomValidatorsService } from '../../../shared/validators/custom-validator.service';
import { addTimestamp } from './../../../_helper/addTimestamp';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../_component/loader/loader.service';
import { dropzoneConfig } from '../../../_global/dropzone.config';
import { IMAGE_DIMENSION } from '../../../_global/image.config';
import { froalaConfig } from '../../../_global/froala.config';
// import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
export interface ProductModel {
    merchant_id: string;
    status: string;
    visibility: string;
    brief: {
        name?: string;
        short_description: string;
        price: number;froalaEditor
        discount: boolean;
        category: string;
        images: [string]
    };
    detail: {
        long_description: string,
        product_brand: string,
        barcode: string;
        sku: string;
    };
    pricing: {
        discount_rate: number,
        final_price: number,
        min_price: number,
        max_price: number,
        discount_price: number,
    };
    stock: {
        qty: number,
        min_qty: number,
    };
}
@Component({
    selector: 'az-product-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.scss'],
    providers: [ProductService, NewProductService],
    entryComponents: [ButtonAssignComponent]
})

export class ProductDetailComponent implements OnInit {
    @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;
    private product;
    private picCount = 0;
    private product_id = '';
    private disable = true;
    public editorOptions;
    private cateAttArray;
    private productDetailForm: FormGroup;
    private productCateForm: FormGroup;
    private productAttributeForm: FormGroup;
    private dropzoneConfig: DropzoneConfigInterface = {};
    private category;
    public optionControlModel: number[];
    private baseUrl = `${environment.apiUrl}/uploads/user/${this._authService.getUser()}/products/`;
    private images = [];
    private selected = [];
    private productAttributes = [];
    private variantRowSelected;
    private mainCategory = [];
    private subCategory = [];
    private subSubCategory = [];
    private outdated = [];
    private variants = [];
    private variantImages = [];
    private source: LocalDataSource;
    private isActive = false;
    private oldForm: any;
    private config = {
        variants: [],
        source: undefined,
        default: undefined
    };
    private settings = {
        actions: {
            add: false,
        },
        delete: {
            confirmDelete: true,
        },
        edit: {
            confirmSave: true
        },
        columns: {
            option_name: {
                title: 'Option',
                editable: false
            },
            option_value: {
                title: 'Value',
                editable: false,
                // filter: false
            },
            price: {
                title: 'Variant Price',

            },
            discount_rate: {
                title: 'Discount Rate',
                editable: false
            },
            discount_price: {
                title: 'Discounted Price',
                editable: false
            },
            stock: {
                title: 'Qty',
            },
            sku: {
                title: 'SKU',
                editable: true
            },
            assignImage:
                {
                    title: 'Assign Image',
                    editable: false,
                    type: 'custom',
                    renderComponent: ButtonAssignComponent,
                    onComponentInitFunction: (instance) => {
                        this.buttonAssignInstances.push(instance);
                        instance.save.subscribe(row => this.variantRowSelected = row);
                    }
                }
        }
    };
    private buttonAssignInstances: Array<ButtonAssignComponent> = [];
    constructor(
        private _productService: ProductService,
        private _categoryService: CategoryService,
        private _dragulaService: DragulaService,
        private _newProductService: NewProductService,
        private _router: Router,
        private _location: Location,
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _toastr: ToastrService,
        private _loader: LoaderService,
        private formBuilder: FormBuilder,
        private sanitizer: DomSanitizer,
        private pipeDecimal: DecimalPipe,

    ) {
        // Wizardz
        this.productCateForm = this.formBuilder.group(
            {
                mainCategory: [''],
                subCategory: [''],
                subSubCategory: ['']
            }
        );
        this.productDetailForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.maxLength(50)]],
            sku: ['', [Validators.required, Validators.maxLength(15)]],
            barcode: ['', Validators.maxLength(15)],
            long_description: [''],
            short_description: [''],
            product_brand: ['', [Validators.maxLength(50)]],
            price: ['', [Validators.required, CustomValidatorsService.dollar(), CustomValidatorsService.min(0.1)]],
            discount_rate: ['', CustomValidatorsService.range(0, 100, false)],
            discount: [false, ],
            stock: ['', [Validators.required, CustomValidatorsService.integer(), CustomValidatorsService.min(1)]],
        });
        this.productDetailForm.controls['discount'].valueChanges.subscribe((discount) => {
            this.productDetailForm.controls['discount_rate'].setValidators(discount ? CustomValidatorsService.range(0, 100, false) : null);
            this.productDetailForm.controls['discount_rate'].updateValueAndValidity();
        });

        this.source = new LocalDataSource();
        this.config.source = this.source;
    }
    ngOnInit() {
        this._loader.show();
        const {
            apiUrl
        } = environment;
        this._activatedRoute.params.subscribe(
            params => {
                this.product_id = params.id;
                this.baseUrl += params.id;
                const header = this._authService.getAuthHeader();
                this.editorOptions = {
                    ...froalaConfig,
                    imageUploadURL: `${apiUrl}/merchant/thumbnail/description?product_id=${this.product_id}`,
                    requestHeaders: header
                };
                this.dropzoneConfig = Object.assign(dropzoneConfig, {
                    headers: header,
                    url: `${environment.apiUrl}/merchant/thumbnail/slider/${this.product_id}`,
                });
                this._productService.getProductDetail(
                    `page=1&limit=1&select=brief/detail/stock/pricing/is_active&product_id=${params.id}`
                ).subscribe(
                    payload => {
                        this.product = payload;
                        const {
                            brief: {
                                category,
                                images,
                                attributes: attributesOfProduct
                            },
                            is_active
                        } = this.product;
                        this.category = category.split(',').join('/');
                        this.productCateForm.patchValue(this.getProductCate(category));
                        this.productDetailForm.patchValue(this.getProductDetail(this.product));
                        this.images = this.getProductPic(images);
                        this.isActive = is_active;
                        console.log(this.product);
                        this.config.default = this.productDetailForm.value;
                        this.loadCategoryAttributeAndVariant(category)
                            .subscribe((data) => {
                                const { attributes, variant } = data;
                                this.cateAttArray = attributes;
                                this.productAttributeForm = this.formize(attributes);
                                (attributesOfProduct || []).forEach(e => {
                                    if (!e.variant) {
                                        const fieldsNotFound = this.setForm(this.productAttributeForm, e.name, e.value);
                                        if (fieldsNotFound) { this.outdated.push(fieldsNotFound); }
                                    }
                                });
                                if (variant.length) {
                                    this.variants = variant;
                                    this.config.variants = this.variants;
                                }
                            });
                        this.loadProductVariant().subscribe((s: any) => {
                            this._loader.hide();
                            this.source.load(s);
                        }, e => this._toastr.error(e.error.message)
                        );
                    },
                    onError => {
                        this._toastr.error('Server Error');
                    }
                );
            });
    }

    private loadProductVariant() {
        // alert("Hi");
        const { discount_rate } = this.productDetailForm.value;
        return this._productService.getProductVariant(this.product_id)
            .map((s: any) => s.map(e => (
                {
                    ...e,
                    // 'thumbnailUrl': `${this.baseUrl}/${e.image}`,
                      'thumbnailUrl': `${this.baseUrl}/${e.image}`,
                    discount_rate,
                    discount_price: this.pipeDecimal.transform(e.price * (1 - discount_rate / 100), '1.2-2')
                }
            )));
    }
    private arrayToObject(array, toBeKey, toBeValue) {
        const o: any = {};
        array.forEach(a => {
            const key = a[toBeKey];
            const value = a[toBeValue];
            o[key] = value;
        });
        return o;
    }
    private setForm(form: FormGroup, name, value) {
        const control = form.controls[name];
        if (control) {
            control.setValue(value);
            return undefined;
        }
        return {
            name,
            value
        };
    }

    private onUploadError(e) {
        this._toastr.error(e[1].message);

    }
    private onUploadSuccess(e) {
        this._toastr.success('Upload successful');
        this.images.push({
            url: addTimestamp(`${this.baseUrl}/${e[0].name}`),
            name: e[0].name
        });
    }
    private onAddedFile($event) {

    }
    private selectImage(imgName) {
        this.variantRowSelected['image'] = imgName;
        this.variantRowSelected['thumbnailUrl'] = addTimestamp(`${this.baseUrl}/${imgName}`);
    }
    private doneSelect() {
        const { _id } = this.variantRowSelected;
        this._productService.assignVariantPic({
            'variant_id': _id,
            ...this.variantRowSelected
        }).subscribe(
            payload => {
                this._toastr.success('Variant picture assigned!');
                const buttonInstanceSelected = this.buttonAssignInstances.find(i => i._rowData._id === _id);
                buttonInstanceSelected.rowData = this.variantRowSelected;
            },
            e => {
                this._toastr.error(e.error.message);
            }
        );
        return;
    }
    private removePic(e) {
        e.stopPropagation();
        this._newProductService.deletePhoto({ 'remove': e.upload.filename + '.png' }, this.product_id).subscribe(
            (payload: any) => this._toastr.error(payload.message),
            e => this._toastr.error(e.error.message)
        );
    }
    private optionOnChange() {
        // console.log('option', this.optionControlModel);
    }
    private loadCategoryAttributeAndVariant(category) {
        return this._productService.getOneCategory(category).map(
            (payload: any) => {
                console.log(payload);
                const a = [];
                const v = [];
                if (payload[0].attributes) {
                    payload[0].attributes.forEach(e => e.variant ? v.push(e) : a.push(e));
                }
                return {
                    attributes: a,
                    variant: v
                };
            },
            e => this._toastr.error(e.error.message)
        );
    }
    private formize(array) {
        const formGroup = {};
        array.forEach(a => formGroup[a.name] = new FormControl(a.value[0]));
        return new FormGroup(formGroup);
    }
    private formizeArray(array) {
        if (!array) { return; }
        const formGroup = {};
        array.forEach(a => {
            undefined !== a.value ? formGroup[a.name] = this.formizeArray(a.value) : formGroup[a] = new FormControl('');
        });
        return new FormGroup(formGroup);
    }
    private submitAttribute() {

        const dataToSumit = {
            'brief': {

            },
            'product_id': this.product_id,
        };
        console.log(dataToSumit);
        this._newProductService.updateDetailProduct(dataToSumit).subscribe(
            payload => this._toastr.success('Attribute updated'),
            e => this._toastr.error(e.error.message)
        );
    }
    private getAttribute() {
        return Object.keys(this.productAttributeForm.value).map(
            key => {
                return {
                    'name': key,
                    'value': this.productAttributeForm.value[key]
                };
            });
    }
    private getProductCate(data) {
        const productCate = data.split(',');
        return {
            'mainCategory': productCate[1],
            'subCategory': productCate[2],
            'subSubCategory': productCate[3]
        };
    }
    private getProductDetail(data) {
        const {
            brief,
            detail,
            stock,
            pricing
        } = data;
        const x: any = {};
        if (brief) {
            Object.assign(x, brief);
        }
        if (detail) {
            Object.assign(x, detail);
        }
        if (stock) {
            const { qty } = stock;
            x.stock = qty;
        }
        if (pricing) {
            const { discount_rate } = pricing;
            x.discount_rate = discount_rate;
        }
        return x;
    }
    private updateProduct() {
        const {
            valid,
            value: {
                barcode,
                sku,
                product_brand,
                long_description,
                short_description,
                name,
                price,
                discount,
                stock,
                discount_rate
            }
        } = this.productDetailForm;
        this.disable = true;
        if (!valid) { return this._toastr.error('Invalid form'); }

        if (this.productDetailForm.get('price').dirty) {
            if (!confirm(
                `Warn: Changing base product price will set all variant price equal to new base product price.Are you sure to proceed ?`
            )) {
                console.log(this.oldForm);
                return this.productDetailForm.reset(this.oldForm);
            }
        }

        const dataToSubmit = {
            'product_id': this.product_id,
            'detail': {
                barcode,
                sku,
                product_brand,
                long_description,
            },
            'brief': {
                name,
                price,
                discount,
                'attributes': this.getAttribute(),
                short_description
            },
            'stock': {
                'qty': stock
            },
            'pricing': {
                'discount_rate': discount ? discount_rate : 0
            }

        };
        this._newProductService.updateDetailProduct(dataToSubmit).subscribe(
            payload => {
                this.loadProductVariant().subscribe((s: any) => {
                    this.source.load(s);
                    this._toastr.success('Updated', 'Product detail');
                });
                this.config = {
                    ...this.config,
                    default: this.productDetailForm.value
                };
                console.log(this.productDetailForm.value);
            },
            e => this._toastr.error(e.error.message)
        );
    }
    private updateVariant(event) {
        const { _id, price, discount_rate, sku, stock } = event.newData;
        const dataToSubmit = {
            variant_id: _id,
            price,
            sku,
            stock
        };
        this._productService.updateVariant(dataToSubmit).subscribe(
            payload => {
                this.loadProductVariant().subscribe(
                    (s: any) => {
                        this.source.load(s);
                        event.confirm.resolve();
                        // this.variantOptions.reset()
                        this._toastr.success('Updated!', 'Variant');
                    },
                    e => this._toastr.error(e.error.message)
                );
            },
            e => {
                event.confirm.reject(e);
                const error = e.error.message;
                this._toastr.error(Array.isArray(error) ? error[0].msg : error);
            }
        );
    }
    private onDeleteConfirm(event) {
        return this._productService.deleteVariant({ variant_id: event.data._id })
            .subscribe((payload: any) => {
                this._toastr.success(payload.message);
                event.confirm.resolve();
            },
                e => this._toastr.error(e.error.message));
    }
    private delProduct() {
        if (confirm('Are you sure you want to remove this product? This action requires review and is irrevocable!')) {
            this._productService.deleteProduct({ product: [this.product_id] }).subscribe(
                payload => {
                    this._toastr.success('Product delete Successful');
                    this._location.back();
                },
                e => this._toastr.error(e.error.message)
            );
        }
    }
    private getProductPic(images) {
        return images.map(imageName => ({
            url: `${this.baseUrl}/${imageName}`,
            name: imageName
        }));
    }
    private removeImage(event, i) {
        event.stopPropagation();
        this._newProductService.deletePhoto({ 'remove': i.name }, this.product_id).subscribe(
            payload => {
                const imageToRemove = this.images.findIndex(e => e.name === i.name);
                this.images.splice(imageToRemove, 1);
            },
            e => this._toastr.error(e.error.message));
    }

    private changeProductActiveStatus() {
        if (!confirm(`Are you sure you want to ${this.isActive ? 'inactivate' : 'activate'} your product?`)) { return; }
        this._productService.changeProductActiveStatus(`product_id=${this.product_id}`).subscribe(
            s => this.isActive = !this.isActive,
            e => this._toastr.error(e)
        );
    }
    private addVariants(newVariants) {
        const dataToSubmit = {
            'variants': newVariants,
            'product_id': this.product_id
        };
        this._productService.uploadVariant(dataToSubmit).subscribe(
            async (payload: any) => {
                this._toastr.success(`Variant upload success! ${payload.message}`);
                this.variants = newVariants;
                this.source.load([...payload.data, ...await this.source.getAll()]);
            },
            e => this._toastr.error(e.error.message)
        );

    }
    private startEditProduct() {
        this.disable = null;
        this.oldForm = Object.assign({}, this.productDetailForm.value);
    }
    private cancelEditProduct() {
        this.disable = true;
        return this.productDetailForm.reset(this.oldForm);
    }
}

