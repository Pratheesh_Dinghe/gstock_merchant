import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { AuthService } from '../../shared/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()


export class DashboardService {
    private url = environment.apiUrl
    private authHeader;


    constructor(private http: HttpClient, private _authService: AuthService) {
        this.authHeader = _authService.getAuthHeader();
    }
    weatherData = [
        { day: 'Sunday', icon: 'clear-day', degree: '18° / 22°' },
        { day: 'Monday', icon: 'partly-cloudy-day', degree: '14° / 16°' },
        { day: 'Tuesday', icon: 'cloudy', degree: '8° / 12°' },
        { day: 'Wednesday', icon: 'rain', degree: '4° / 6°' },
        { day: 'Thursday', icon: 'sleet', degree: '-1° / 3°' },
        { day: 'Friday', icon: 'snow', degree: '-3° / -1°' },
        { day: 'Saturday', icon: 'fog', degree: '-1° / 2°' }
    ]

    public getWeatherData(): Object {
        return this.weatherData;
    }

   public getSummary() {
        return this.http.get(this.url + '/merchant/report', { headers: this.authHeader })
    }


}
