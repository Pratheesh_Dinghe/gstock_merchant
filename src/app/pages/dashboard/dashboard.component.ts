import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from '../../app.config';
import { DashboardService } from './dashboard.service';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { AuthService } from '../../shared/auth/auth.service';
import { LoaderService } from '../../_component/loader/loader.service';

@Component({
    selector: 'az-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [DashboardService]
})
export class DashboardComponent {
    public config: any;
    public configFn: any;
    public bgColor: any;
    public date = new Date();
    public netSales = 0;
    public weatherData: any;
    public report;

    constructor(
        private _appConfig: AppConfig,
        private _dashboardService: DashboardService,
        private _authService: AuthService,
        private _loader: LoaderService
    ) {
        this._loader.show();
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.weatherData = _dashboardService.getWeatherData();
        this._dashboardService.getSummary().subscribe(
            (onSuccess: any) => {
                this._loader.hide();
                this.report = onSuccess['total'];
                console.log('Dash', this.report);
                if (this.report.order.all[0]) {
                    this.netSales = this.report.order.all[0].amt - this.report.comission.approved[0].total;
                }

            },
            onError => {
                alert('Server Error');
                this._loader.hide();
            }
        );
    }

    ngAfterViewChecked() {

    }
    ngOnInit(){
     //  console.log( this._authService.getUser());
      
          
    }
}
