import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class PagesService {
    private url = environment.apiUrl;
    constructor(private http: Http) {

    }
}
