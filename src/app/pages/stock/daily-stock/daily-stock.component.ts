import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/auth/auth.service';
import { LocalDataSource } from "ng2-smart-table";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StockService } from '../stock.service';
import { LoaderService } from '../../../_component/loader/loader.service';
import { flatten } from 'lodash';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
//import { Ng2SmartTableModule } from 'ng2-smart-table';

//import { CustomValidatorsService } from "app/shared/custom-validator.service";
//import { AuthService } from '../../shared/auth/auth.service';

@Component({
  selector: 'az-daily-stock',
  templateUrl: './daily-stock.component.html',
  styleUrls: ['./daily-stock.component.scss']
})
export class DailyStockComponent implements OnInit {
  dailyStockForm: FormGroup;
  dailyStock_date;
  name;
  sku;
  qty;
  userqty;
  dailyStock:{};
  public item: any = [];
  public dailystock:any=[];
  source: LocalDataSource;
  private settings = {
   // hideSubHeader: true,
    actions: {
      delete: false,
      edit: true,
      add: false,
    },
    edit: {
      editButtonContent: 'EDIT ',
      saveButtonContent: 'SAVE ',
      cancelButtonContent: 'CANCEL ',
      confirmSave: true,
    },

      columns: {
      'slNo': {
        title: 'Sl No',
        valuePrepareFunction(value, row, cell) { 
          return cell.row.index + 1; },
       },
       
       "sku": {
        title: "Sku",
        editable: false,
      },

      "name": {
        title: "Item Name",
        editable: false,
      },

      "current_qty": {
        title: "Current Stock",
        editable: false,
      },
      Updated_qty: {
        editable: true,
        title: "Qty",
      },

      // userqty: {
      //   editable: true,
      //   title: "Qty",
      // },
    }
  };
  paginationComponent: any;
  origin: any;
  query: any;
  Current_qty: any;
  Updated_qty: any;


  constructor( private _loader: LoaderService,private _authService: AuthService, private _formBuilder: FormBuilder, private _stockService: StockService) {
    this.dailyStockForm = _formBuilder.group({
      dailyStock_date: ["", Validators.required],

    });
    this.source = new LocalDataSource();
  }
   getNowDate() {
     var returnDate = "";
     var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth() + 1;
     var yyyy = today.getFullYear();
     returnDate += `${yyyy}-`;
     if (mm < 10) {
       returnDate += `0${mm}-`;
     } else {
       returnDate += `${mm}-`;
     }
     if (dd < 10) {
       returnDate += `0${dd}`;
     } else {
       returnDate += `${dd}`;
     }
 
     return returnDate;
   }
   onSaveConfirm(event) {
    console.log(event.newData)
 
    this.name=event.newData.name;
    this.sku=event.newData.sku;
    this.qty=event.newData.current_qty;
    //this.userqty=event.newData.userqty;
    this.Updated_qty=event.newData.Updated_qty;
  
    this._stockService.dailyStock({ 
      name:this.name,
      sku:this.sku,
      current_qty:this.qty,
      Updated_qty:this.Updated_qty,
     
          }).subscribe(data => {
      console.log(data);
      event.confirm.resolve();
    })

  }
  ngOnInit() {
       this.dailyStock_date = this.getNowDate();
      console.log(this.dailyStock_date);
      console.log(this._authService.getUser());
      console.log("ID",this._authService.getUser())
     this._stockService.productList({id: this._authService.getUser()}).subscribe(data => {
     // this._stockService.productList().subscribe(data => {
        console.log(data);
        this.item = data;
        console.log(this.item);
         this.item.forEach(element => {
          this.dailystock.push({
            name:element.brief.name,
            sku:element.detail.sku,
            current_qty:element.stock.qty
          })
        });
      //  this.source.load(this.item);
      this.source.load(this.dailystock);
      })
  }

}
  // columns: {
    //   'slNo': {
    //     title: 'Sl No',
    //     valuePrepareFunction(value, row, cell) { 
    //       return cell.row.index + 1; },
         
    //   },

    //   "detail": {
    //     title: "Item Code",
    //     valuePrepareFunction(value, row, cell) {
    //       return value.sku;
    //     },
       
    //   },
    //   "brief": {
    //     title: "Item Name",
    //      valuePrepareFunction(value, row, cell) {
    //        return value.name;
         
    //     }
    //   },
    //   "stock": {
    //     title: "Current Stock",
    //     valuePrepareFunction(value, row, cell) {
    //       return value.qty;
    //     }
    //   },
    //   userqty: {
    //     editable: true,
    //     title: "Qty",
    //   },


    // }