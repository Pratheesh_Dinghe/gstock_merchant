import { Injectable } from "@angular/core";
import { AuthService } from "../../shared/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
//import { Http, Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';

@Injectable()
export class StockService {
  private url = environment.apiUrl;
  private adminurl = environment.adminurl;
  authHeader: any;
  constructor(
    private httpClient: HttpClient,
    private _authService: AuthService
  ) {
    // this.authHeader = _authService.getHeaders();
  }
  /* productList(body) {
    console.log(body)
    return this.httpClient.post(this.url + "/admin/product/getlist", body);
    //  headers: this.authHeader
   }*/

  productList(body) {
    return this.httpClient.post(this.adminurl + "/admin/product/getlist", body);
    //  headers: this.authHeader
  }

  public dailyStock(body) {
    console.log(body);
    return this.httpClient.post(this.adminurl + "/admin/dailystock/", body);
    //  headers: this.authHeader
  }
  public SaveStockManaging(body) {
    console.log(body);
    return this.httpClient.post(this.adminurl + "/admin/product/SaveStockManaging/", body);
    //  headers: this.authHeader
  }

  public search_Product(query) {
    // alert(this.url + "/merchant/product" + query);
    return this.httpClient.get(this.adminurl + "/merchant/product" + query, {
      headers: this.authHeader
    });
  }
}
