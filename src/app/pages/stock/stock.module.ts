import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockService } from './stock.service';
import { RouterModule } from '@angular/router';
import { Ng2SmartTableModule } from 'ng2-smart-table';
//import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DailyStockComponent } from './daily-stock/daily-stock.component';
import { StockManagingComponent } from './stock_managing/stock-managing.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const picCount = 0;
export const routes = [
  { path: '', redirectTo: 'daily-stock', pathMatch: 'full' },
  { path: 'daily-stock', component: DailyStockComponent, data: { breadcrumb: 'Daily Stock' } },
  { path: 'stock-managing', component: StockManagingComponent, data: { breadcrumb: 'Stock Managing' } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [DailyStockComponent, StockManagingComponent],
  providers: [StockService]
})
export class StockModule { }
