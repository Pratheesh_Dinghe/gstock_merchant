import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockManagingComponent } from './stock-managing.component';

describe('DailyStockComponent', () => {
  let component: StockManagingComponent;
  let fixture: ComponentFixture<StockManagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockManagingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockManagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
