import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PagesComponent } from './pages.component';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule', data: { breadcrumb: 'Dashboard' } },
            { path: 'maps', loadChildren: 'app/pages/maps/maps.module#MapsModule', data: { breadcrumb: 'Maps' } },
            // { path: 'charts', loadChildren: 'app/pages/charting/charting.module#ChartingModule', data: { breadcrumb: 'Charts' } },
            // { path: 'ui', loadChildren: 'app/pages/ui/ui.module#UiModule', data: { breadcrumb: 'UI' } },
            // { path: 'tools', loadChildren: 'app/pages/tools/tools.module#ToolsModule', data: { breadcrumb: 'Tools' } },
            { path: 'mail', loadChildren: 'app/pages/mail/mail.module#MailModule', data: { breadcrumb: 'Mail' } },
            // { path: 'calendar', loadChildren: 'app/pages/calendar/calendar.module#CalendarModule', data: { breadcrumb: 'Calendar' } },
            // {
            //     path: 'form-elements',
            //     loadChildren: 'app/pages/form-elements/form-elements.module#FormElementsModule',
            //     data: { breadcrumb: 'Form Elements' }
            // },
            // { path: 'tables', loadChildren: 'app/pages/tables/tables.module#TablesModule', data: { breadcrumb: 'Tables' } },
            // { path: 'editors', loadChildren: 'app/pages/editors/editors.module#EditorsModule', data: { breadcrumb: 'Editors' } },
            // { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } },
            // { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' } },
            { path: 'order', loadChildren: 'app/pages/order/order.module#OrderModule', data: { breadcrumb: 'Orders' } },
            { path: 'products', loadChildren: 'app/pages/products/products.module#ProductsModule', data: { breadcrumb: 'Products' } },
            { path: 'stock', loadChildren: 'app/pages/stock/stock.module#StockModule', data: { breadcrumb: 'stock' } },
            {
                path: 'promotion',
                loadChildren: 'app/pages/promotion/promotion.module#PromotionModule',
                data: { breadcrumb: 'Promotion Management' }
            },
            {
                path: 'setting',
                loadChildren: 'app/pages/setting/setting.module#SettingModule',
                data: { breadcrumb: 'Account Setting' }
            },
            {
                path: 'report',
                loadChildren: 'app/pages/report/report.module#ReportModule',
                data: { breadcrumb: 'Reporting' }
            },
            {
                path: 'commission',
                loadChildren: 'app/pages/commission/commission.module#CommissionModule',
                data: { breadcrumb: 'Commission' }
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
