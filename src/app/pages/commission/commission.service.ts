import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService } from '../../shared/auth/auth.service';


@Injectable()
export class CommissionService {
  private url = environment.apiUrl;
  public authHeader;

  constructor(private http: HttpClient, private _authService: AuthService) {
    this.authHeader = this._authService.getAuthHeader();
  }
  public getMerchantProfile() {
    return this.http.get(this.url + '/user/account/detail?select=profile/company/contact', { headers: this.authHeader });

  }
  public imageUpload(body) {
    return this.http.put(this.url + '/user/account/detail/profile_picture', body, { headers: this.authHeader });
  }

  public updateMerchantProfile(data) {
    return this.http.put(this.url + '/user/account/detail', data, { headers: this.authHeader });
  }

  public getOrders() {
    return this.http.get(this.url + '/merchant/order', { headers: this.authHeader });
  }


  public getAllPromotion(body = {}) {

    return this.http.post(this.url + '/admin/promotion/search', body);
  }
}
