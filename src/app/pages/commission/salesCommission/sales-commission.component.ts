import { errorHandler } from "../../../_helper/errorHandler";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { LocalDataSource, Ng2SmartTableModule } from "ng2-smart-table";
import { CommissionService } from "../commission.service";
import { Router } from "@angular/router";
import { DatePipe, CurrencyPipe } from "@angular/common";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import * as flatten from "flatten-obj";
import * as jsPDF from "jspdf";

@Component({
  selector: "az-sales-commission",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./sales-commission.component.html",
  styleUrls: ["./sales-commission.component.scss"]
})
export class SalesCommissionComponent implements OnInit {
  private orderFilter: FormGroup;
  private orderList = [];
  private totalSales = 0;
  private totalComm = 0;
  private netSales = 0;
  private salesOnhold = 0;
  private salesReleased = 0;
  private salesApproved = 0;
  private salesRejected = 0;
  // private statusFilter = [];

  private source = new LocalDataSource();
  private settings = {
    actions: {
      delete: false,
      edit: false,
      add: false
    },
    columns: {
      _id: {
        title: "Order ID"
      },
      createdAt: {
        title: "Date",
        valuePrepareFunction: date => {
          const raw = new Date(date);
          const formatted = new DatePipe("en-EN").transform(
            raw,
            "dd-MM-yyyy HH:mm"
          );
          return formatted;
        }
      },
      "total.store_ap": {
        title: "Sales Amount",
        valuePrepareFunction: amount => {
          const formatted = new CurrencyPipe("en-EN").transform(amount, "SGD");
          return formatted;
        }
      },
      "total.commission": {
        title: "Gstock Commission",
        valuePrepareFunction: amount => {
          const formatted = new CurrencyPipe("en-EN").transform(amount, "SGD");
          return formatted;
        }
      },
      commission_status: {
        title: "Sales Status"
      }
    }
  };
  constructor(
    private router: Router,
    public _commissionService: CommissionService,
    private formBuilder: FormBuilder
  ) {
    this.orderFilter = formBuilder.group({
      fromDate: ["", Validators.required],
      toDate: ["", Validators.required]
    });

    this._commissionService.getOrders().subscribe(
      (data: Array<any>) => {
        console.log(data);
        this.source.load(
          data.map(order => {
            return flatten()(order);
          })
        );
        this.orderList = data;
        this.orderList.forEach(o => {
          const { commission_status } = o;
          this.totalComm += o.total.commission;
          this.totalSales += o.total.store_ap;
          console.log(commission_status);
          switch (commission_status) {
            case "Hold": {
              this.salesOnhold += o.total.store_ap;
              break;
            }
            case "Approved": {
              this.salesApproved += o.total.store_ap;
              break;
            }
            case "Released": {
              this.salesReleased += o.total.store_ap;
              break;
            }
            case "Rejected": {
              this.salesRejected += o.total.store_ap;
              break;
            }
            default:
              throw new Error("Unknown commision status");
          }
        });
        this.netSales = this.totalSales - this.totalComm;
        console.log("i2", this.totalComm);
      },
      err => errorHandler(err)
    );
  }

  ngOnInit() {}

  // filterSubmit(beginDate: string, endDate: string)
  filterSubmit(formData) {
    const beginDate = formData.value.fromDate;
    const endDate = formData.value.toDate;
    console.log("begin", beginDate);
    console.log("end", endDate);

    if (!(!beginDate && !endDate)) {
      this.source
        .setFilter(
          [
            {
              field: "createdAt",
              search: endDate,
              filter: (value: string, endValue: string) => {
                return new Date(value) <= new Date(endValue);
              }
            }
          ],
          true
        )
        .setFilter([
          {
            field: "createdAt",
            search: beginDate,
            filter: (value: string, beginValue: string) => {
              return new Date(value) >= new Date(beginValue);
            }
          }
        ]);
    } else {
      this.source = new LocalDataSource(
        this.orderList.map(order => {
          return flatten()(order);
        })
      );
    }
  }

  filt;

  private statusChange(status?) {
    // this._promotionService.getAllPromotion({ query: { 'status': status } }).subscribe(
    //     (data: any) => this.source.load(data),
    //     err => errorHandler(err)
    // )
    this.source.setFilter([
      {
        field: "commission_status",
        search: status
      }
    ]);
  }

  private generateReport() {
    const doc = new jsPDF();
    doc.text(105, 20, "Sales Report", null, null, "center");
    doc.text(105, 30, "Date: 01/12/2017-31/12/2017", null, null, "center");
    doc.rect(20, 40, 170, 180);
    doc.line(20, 50, 190, 50);
    doc.line(20 + 34, 40, 20 + 34, 220);
    doc.line(20 + 34 * 2, 40, 20 + 34 * 2, 220);
    doc.line(20 + 34 * 3, 40, 20 + 34 * 3, 220);
    doc.line(20 + 34 * 4, 40, 20 + 34 * 4, 220);
    doc.setFontSize(12);
    doc.text(23, 47, "OrderId");
    doc.text(23 + 34 * 1, 47, "Date");
    doc.text(23 + 34 * 2, 47, "Sales");
    doc.text(23 + 34 * 3, 47, "Gstock Comm");
    doc.text(23 + 34 * 4, 47, "Status");

    this.orderList.forEach((p, i) => {
      console.log(p);
      doc.text(23, 47 + 10 * (i + 1), p._id.slice(16));
      doc.text(
        21 + 34 * 1,
        47 + 10 * (i + 1),
        new DatePipe("en-EN").transform(p.createdAt, "dd-MM-yyyy HH:mm") + ""
      );
      doc.text(
        23 + 34 * 2,
        47 + 10 * (i + 1),
        new CurrencyPipe("en-EN").transform(p.total.store_ap, "SGD") + ""
      );
      doc.text(
        23 + 34 * 3,
        47 + 10 * (i + 1),
        new CurrencyPipe("en-EN").transform(p.total.commission, "SGD") + ""
      );
      doc.text(23 + 34 * 4, 47 + 10 * (i + 1), p.commission_status + "");
      // doc.text(150, 240, 'Total Amount');
      // doc.line(30, 100 + 10  *(i + 1), 180, 100 + 10  *(i + 1));
    });

    doc.text(
      20,
      240,
      "Total Sales " +
        new CurrencyPipe("en-EN").transform(this.totalSales, "SGD")
    );
    doc.text(
      20,
      250,
      "Gstock Commission " +
        new CurrencyPipe("en-EN").transform(this.totalComm, "SGD")
    );
    doc.text(
      20,
      260,
      "Net Sales " + new CurrencyPipe("en-EN").transform(this.netSales, "SGD")
    );

    doc.save("autoprint.pdf");
  }
  private exportReport() {
    const csvObject = [];
    let smallArray = {};
    const summaryArray = {
      "Total Sales": 34343,
      "Total Commission": 3434
    };
    this.orderList.forEach(p => {
      smallArray = {
        "Order id": p._id.slice(16),
        "Created Date": new DatePipe("en-EN").transform(
          p.createdAt,
          "dd-MM-yyyy HH:mm"
        ),
        Sales: p.total.store_ap.toFixed(2),
        "Gstock Commission": p.total.commission.toFixed(2),
        "Commission Status": p.commission_status
      };
      csvObject.push(smallArray);
    });

    // this.csvService.download(csvObject, 'Sales_Report');
  }
}
