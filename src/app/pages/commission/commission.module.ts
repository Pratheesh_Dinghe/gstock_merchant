import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommissionService } from './commission.service';
import { SalesCommissionComponent } from './salesCommission/sales-commission.component';
import { CateCommissionComponent } from './cateCommission/cate-commission.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { NgDatepickerModule } from 'ng2-datepicker';
import { UtilComponentsModule } from '../../theme/util-components/util.module';

export const routes = [
  { path: '', redirectTo: 'sales-commission', pathMatch: 'full' },
  { path: 'sales-commission', component: SalesCommissionComponent, data: { breadcrumb: 'Sales Commission' } },
  { path: 'cate-commission', component: CateCommissionComponent, data: { breadcrumb: 'Category Commission' } }
];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgDatepickerModule,
    UtilComponentsModule
  ],
  declarations: [SalesCommissionComponent, CateCommissionComponent],
  providers: [CommissionService]
})

export class CommissionModule { }
