import { AuthService } from '../shared/auth/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
import { BackTopComponent } from '../theme/components/back-top/back-top.component';
import { BreadcrumbComponent } from '../theme/components/breadcrumb/breadcrumb.component';
import { MenuComponent } from '../theme/components/menu/menu.component';
import { MessagesComponent } from '../theme/components/messages/messages.component';
import { NavbarComponent } from '../theme/components/navbar/navbar.component';
import { SidebarComponent } from '../theme/components/sidebar/sidebar.component';
import { DirectivesModule } from '../theme/directives/directives.module';
import { PipesModule } from '../theme/pipes/pipes.module';
import { PagesComponent } from './pages.component';
import { routing } from './pages.routing';
import { PagesService } from './pages.service';
//import { DailyStockComponent } from './stock/daily-stock/daily-stock.component';




@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    // .forRoot(PERFECT_SCROLLBAR_CONFIG),
    DirectivesModule,
    PipesModule,
    routing,
    FormsModule,
    ReactiveFormsModule,

  ],
  declarations: [
    PagesComponent,
    MenuComponent,
    SidebarComponent,
    NavbarComponent,
    MessagesComponent,
    BreadcrumbComponent,
    BackTopComponent,
    //DailyStockComponent,
  ],
  providers: [PagesService]
})
export class PagesModule { }
