import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class ReportService {
  private url = environment.apiUrl
  private headers = new Headers({ 'Content-Type': 'application/json', 'authorization': 'JWT ' + window.sessionStorage.accessToken });

  constructor(private http: HttpClient) {

  }

  public getAllPromotion(body = {}) {
    return this.http.post(this.url + '/admin/promotion/search', body)
  }
}
