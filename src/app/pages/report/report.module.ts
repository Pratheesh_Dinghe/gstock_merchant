import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReportService } from './report.service';
import { ReportSalesComponent } from './report-sales/report-sales.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { NgDatepickerModule } from 'ng2-datepicker';

export const routes = [
  { path: '', redirectTo: 'report-sales', pathMatch: 'full' },
  { path: 'report-sales', component: ReportSalesComponent, data: { breadcrumb: 'Sales Report' } }

  // { path: 'promotion-detail/:id', component: PromotionDetailComponent, data: { breadcrumb: 'Promotion Detail' } },

];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgDatepickerModule,
  ],
  declarations: [ReportSalesComponent],
  providers: [ReportService]
})

export class ReportModule { }
