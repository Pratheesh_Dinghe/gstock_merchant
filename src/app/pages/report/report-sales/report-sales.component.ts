import { errorHandler } from '../../../_helper/errorHandler';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, Ng2SmartTableModule } from 'ng2-smart-table';
import { ReportService } from '../report.service';
import { Router } from '@angular/router';

@Component({
    selector: 'az-report-sales',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './report-sales.component.html',
    styleUrls: ['./report-sales.component.scss']
})
export class ReportSalesComponent implements OnInit {
    private source = new LocalDataSource();
    private settings = {
        actions: {
            add: false,
            edit: false,
            delete: false

        },


        columns: {
            promoCode: {
                title: 'Promo Code',

            },
            status: {
                title: 'Status'
            },
            used: {
                title: 'Used',

            },
            start: {
                title: 'start',

            },
            end: {
                title: 'end',

            }

        }
    };
    private promoData = [];

    constructor(private router: Router, private _reportService: ReportService) {
    }

    ngOnInit() {
        
    }

 


}
