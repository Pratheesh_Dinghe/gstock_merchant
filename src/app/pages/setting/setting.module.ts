import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SettingService } from './setting.service';
import { AccountSettingComponent } from './accountSetting/account-setting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { NgDatepickerModule } from 'ng2-datepicker';
import { UtilComponentsModule } from '../../theme/util-components/util.module';

export const routes = [
  { path: '', redirectTo: 'account-setting', pathMatch: 'full' },
  { path: 'account-setting', component: AccountSettingComponent, data: { breadcrumb: 'Account Setting' } }
  // { path: 'promotion-detail/:id', component: PromotionDetailComponent, data: { breadcrumb: 'Promotion Detail' } },
];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgDatepickerModule,
    UtilComponentsModule
  ],
  declarations: [AccountSettingComponent],
  providers: [SettingService]
})

export class SettingModule { }
