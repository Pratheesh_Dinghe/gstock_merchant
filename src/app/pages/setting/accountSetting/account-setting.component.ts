import { errorHandler } from "../../../_helper/errorHandler";
import { addTimestamp } from "../../../_helper/addTimestamp";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { LocalDataSource, Ng2SmartTableModule } from "ng2-smart-table";
import { SettingService } from "../setting.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { AppState } from "../../../app.state";

@Component({
  selector: "az-account-setting",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./account-setting.component.html",
  styleUrls: ["./account-setting.component.scss"]
})
export class AccountSettingComponent implements OnInit {
  private storeInfom: FormGroup;
  private accInfom: FormGroup;
  private passwordChange = false;
  private editMerchant = false;
  private merchantProfile = {};
  private storeImage: any;
  private storeBanner: any;
  private storeBarImage: any;
  private selectedFiles: FileList;
  currentFileUpload: File;
  constructor(
    private _state: AppState,
    private router: Router,
    private _settingService: SettingService,
    private formBuilder: FormBuilder
  ) {
    this.storeInfom = formBuilder.group({
      storeName: ["", Validators.required],
      storeDisc: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(250)])
      ],
      contactNo: ["", Validators.required],
      address: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(100)])
      ],
      postal_code: ["", Validators.required],
      bank_name: ["", Validators.required],
      bank_account: ["", Validators.required],
      company_name: ["", Validators.required],
      reg_no: [""]
    });
    this.accInfom = formBuilder.group({
      email: [""],
      firstName: ["", Validators.required],
      newPassword: [""]
    });
  }

  ngOnInit() {
    this._settingService.getMerchantProfile().subscribe(
      (data: any) => {
        console.log(data);
        const { company, contact, location, store, profile, bank } = data;
        this.merchantProfile = {
          company_name: company ? company.company_name : "",
          reg_no: company ? company.reg_no : "",
          contactNo: contact ? contact.mobile_no : "",
          postal_code: location ? location.postal_code : "",
          storeName: store ? store.name : "",
          storeDisc: store ? store.description : "",
          firstName: profile ? profile.first_name : "",
          address: location ? location.addressline1 : "",
          bank_account: bank ? bank.account_no : "",
          bank_name: bank ? bank.name : ""
        };

        const baseURL = `${environment.apiUrl}/uploads/uploads/user/${
          data._id
        }`;
        this.storeImage = addTimestamp(`${baseURL}/profile_picture.png`);
        this.storeBanner = addTimestamp(`${baseURL}/store_banner.png`);
        this.storeInfom.patchValue(this.merchantProfile);
        console.log(this.merchantProfile);
        this.accInfom.patchValue(this.merchantProfile);
      },
      err => errorHandler(err)
    );
  }
  storeUpdate(formdata) {
    console.log("storeUpdate");
    const dataToSubmit = {
      company: {
        company_name: formdata.value.company_name,
        reg_no: formdata.value.reg_no
      },
      contact: {
        mobile_no: formdata.value.contactNo,
        skype: formdata.value.skype
      },
      location: {
        addressline1: formdata.value.address,
        postal_code: formdata.value.postal_code
      },
      store: {
        name: formdata.value.storeName,
        description: formdata.value.storeDisc
      },
      bank: {
        name: formdata.value.bank_name,
        account_no: formdata.value.bank_account
      }
    };
    this._settingService.updateMerchantProfile(dataToSubmit).subscribe(
      (data: any) => {
        // if (data["message"] === "Update successul") {
        if (data) {
          alert("Profile updated");
          this.editMerchant = false;
          this._state.notifyDataChanged("update.profile", "merchantProfile");
        }
      },
      err => errorHandler(err)
    );
  }

  profileUpdate(formdata) {
    console.log("submited");
    const dataToSubmit = {
      profile: {
        first_name: formdata.value.firstName
      }
    };
    this._settingService.updateMerchantProfile(dataToSubmit).subscribe(
      (data: any) => {
        if (data === "Update successul") {
          alert("Profile updated");
          this.editMerchant = false;
        } else {
          console.log(data);
        }
      },
      err => {
        errorHandler(err);
        console.log(err);
      }
    );
  }

  fileChange(input, type) {
    const reader = new FileReader();

    console.log(input.file, type);
    // alert(type);
    if (input.target.files.length) {
      //   alert("Heeee");
      const file = input.target.files[0];
      // reader.onload = () => {
      //     'profile' === type ? this.storeImage = reader.result : this.storeBanner = reader.result
      // }
      console.log("FILEEE", file);
      reader.readAsDataURL(file);
      //   const blob = new Blob(input.target.files);
      const blob = new Blob(input.target.files);
      this.selectedFiles = input.target.files;
      this.currentFileUpload = this.selectedFiles.item(0);

      const formData = new FormData();
      console.log("BLOB", this.selectedFiles.item(0));
      // formData.append("profile" === type ? "profilePic" : "storeBanner", blob);
      formData.append("profile" === type ? "profilePic" : "storeBanner", blob);

      // formData.append("file", this.currentFileUpload);
      console.log("HEEEEEEEE", formData);
      formData.append("type", file.type);
      if ("profile" === type) {
        this._settingService.imageUpload(formData).subscribe(
          image => {
            this.storeImage = addTimestamp(this.storeImage);
            console.log("STOREIMAGE", this.storeImage);
            this._state.notifyDataChanged("update.profile", "storeImage");
          },
          err => errorHandler(err)
        );
      } else {
        this._settingService.uploadStoreBanner(formData).subscribe(
          image => {
            this.storeBanner = addTimestamp(this.storeBanner);
            this._state.notifyDataChanged("update.profile", "storeBanner");
          },
          err => errorHandler(err)
        );
      }
    }
  }

  editMerchantF(action) {
    if (action === "edit") {
      this.editMerchant = true;
      console.log("Edit");
    }
    if (action === "cancel") {
      this.editMerchant = false;
    }
  }

  removeImage(): void {
    this.storeImage = "";
  }

  // get storeName(){
  //     return this.storeInfom.get('storeName')
  // }

  // get storeDisc(){
  //     return this.storeInfom.get('storeDisc')
  // }

  changePassword() {
    this.passwordChange = true;
  }
}
