import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { AuthService } from "../../shared/auth/auth.service";

@Injectable()
export class SettingService {
  private url = environment.apiUrl;
  public authHeader;

  constructor(private http: HttpClient, private _authService: AuthService) {
    this.authHeader = this._authService.getAuthHeader();
  }
  public getMerchantProfile() {
    return this.http.get(
      this.url +
        "/user/account/detail?select=profile/company/contact/location/store/bank",
      { headers: this.authHeader }
    );
  }
  public imageUpload(body) {
    console.log("BODYYYYYY", body);
    return this.http.put(this.url + "/user/account/detail", body, {
      headers: this.authHeader
    });
  }

  public updateMerchantProfile(data) {
    const body = data;
    console.log(body)
    return this.http.put(this.url + "/user/account/detail", body, {
      headers: this.authHeader
    });
  }

  // public getAllPromotion(body = {}) {

  //   return this.http.post(this.url + '/admin/promotion/search', body, { headers: this.authHeader });
  // }
  public uploadStoreBanner(body) {
    return this.http.post(this.url + "/merchant/account/store/banner", body, {
      headers: this.authHeader,
      responseType: "text"
    });
  }
}
