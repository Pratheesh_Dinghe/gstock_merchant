// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA_2VsPFcZeNZmCZdYJzeprHJVTw1izGvI',
    authDomain: 'gstock-6dab8.firebaseapp.com',
    databaseURL: 'https://gstock-6dab8.firebaseio.com',
    projectId: 'gstock-6dab8',
    storageBucket: 'gstock-6dab8.appspot.com',
    messagingSenderId: '415884250436'
  },
  apiUrl: 'https://gstock.sg/merchant.gstock',
  froalaEditorKey: '1recwzqA2oz=='
};
