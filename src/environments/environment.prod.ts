export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyA_2VsPFcZeNZmCZdYJzeprHJVTw1izGvI',
    authDomain: 'gstock-6dab8.firebaseapp.com',
    databaseURL: 'https://gstock-6dab8.firebaseio.com',
    projectId: 'gstock-6dab8',
    storageBucket: 'gstock-6dab8.appspot.com',
    messagingSenderId: '415884250436'
  },
  apiUrl: 'https://gstock.sg/merchant.gstock',
};
